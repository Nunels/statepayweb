Versione = "07/02/2012";
// Colonne sulle quali calcolare l'altezza massima
af_cols = [ "#colonna1", "#colonna2", "#colonna1_hp", "#colonna2_hp", "#double_box", "#box_right" ];

// Avvia lo script quando la pagina e' pronta e se la finestra viene ridimensionata
//$( window ).load( af_init );
//$( window ).resize( af_init );

// Ridimensiona l'altezza delle colonne all'altezza massima
function af_init() {

 // Crea un pattern per il selettore con gli id delle colonne
 var group = eval( "$( \"" + af_cols.join( ", " ) + "\" )" );

 // Azzera le proprieta' altezza per ogni colonna
 group.each(
  function() {
   $( this ).attr( "style", "min-height: 0px; height: auto !important; height: 0px;" );
  }
 );

 // Trova l'altezza maggiore tra le colonne
 var max = 0;

 group.each(
  function() {
   max = Math.max( max, $( this ).height() );
  }
 );

 // Imposta l'altezza massima su tutte le colonne
 group.each(
  function() {
   $( this ).attr( "style", "min-height: " + max + "px; height: auto !important; height: " + max + "px;" );
  }
 );

}
