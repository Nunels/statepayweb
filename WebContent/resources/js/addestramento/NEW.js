/*

TODO - Sostituire la funzione absfooter() con R.fun.someHeight() per tutte le pagine
TODO - Se non si utilizza il calendario, eliminare funzioni.js dalle pagine

*/

// Raccoglitore di variabili, eventi e funzioni
var R = R || { vars: {}, ev: {}, fun: {} };

// Decide se avviare il debug degli eventi in una console
R.vars.debug = false;

// Raccolta di funzioni da eseguire appena l'html della pagina è completamente caricato
R.ev.ready = function() {
 R.fun.console( 'ev.ready - start' );
 R.fun.toggleClass( '#menu_princ .menu_princ_sez', 'selected', 'mouseover', 'mouseout' );
 R.fun.toggleClass( '#menu_princ .menu_princ_sez', 'selected', 'focus', 'blur' );
 R.fun.dropdown();
 R.fun.dualtab( '.dualtab.active' );
 R.fun.console( 'ev.ready - end' );
};

// Raccolta di funzioni da eseguire a completamento del rendering della pagina
R.ev.load = function() {
 R.fun.console( 'ev.load - start' );
 R.fun.someHeight( new Array( '#menu_princ_ag', '#menu_princ_dog', '#menu_princ_mon' ), 100 );
 R.fun.someHeight( new Array( '#hp-uni .colonna-1-1', '#hp-uni .colonna-1-2' ) );
 R.fun.console( 'ev.load - end' );
};

// Raccolta di funzioni da eseguire sul ridimensionamento della finestra del browser
R.ev.resize = function() {
 R.fun.console( 'ev.resize - start' );
 R.fun.someHeight( new Array( '#menu_princ_ag', '#menu_princ_dog', '#menu_princ_mon' ), 100 );
 R.fun.someHeight( new Array( '#hp-uni .colonna-1-1', '#hp-uni .colonna-1-2' ) );
 R.fun.console( 'ev.resize - end' );
};

// Ridimensiona le altezze dei box all'altezza maggiore
// selectors: array di selettori in formato jQuery
// minHeight: altezza minima di ridimensionamento delle altezze (0 di default)
R.fun.someHeight = function( selectors, minHeight ) {
 if( R.fun.cssEnabled() ) {
  R.fun.console( 'fun.someHeight - start on: ' + selectors + '; minHeight: ' + ( R.fun.isset( minHeight ) ? minHeight : '0' ) );
  var group = eval( '$( \'' + selectors.join( ', ' ) + '\' )' );
  //group.each( function() { $( this ).attr( 'style', 'min-height: 0px; height: auto !important; height: 0px;' ); } );
  group.each( function() { $( this ).attr( 'style', 'min-height: 0px;' ); } );
  var max = 0;
  group.each( function() { max = Math.max( max, R.fun.isset( minHeight ) ? minHeight : 0, $( this ).height() ); } );
  //group.each( function() { $( this ).attr( 'style', 'min-height: ' + max + 'px; height: auto !important; height: ' + max + 'px;' ); } );
  group.each( function() { $( this ).attr( 'style', 'min-height: ' + max + 'px;' ); } );
  R.fun.console( 'fun.someHeight - max height: ' + max );
  R.fun.console( 'fun.someHeight - end on: ' + selectors );
 }
};

// Attivatore dei menù a comparsa delle lingue
// Se eseguita, disabilita la funzionalità dell'hover definita mediante css, e
// apre e chiude i dropdown mediante click ed è navigabile anche mediante tastiera
R.fun.dropdown = function() {
 if( R.fun.cssEnabled() ) {
  $( '.dropdown' ).removeClass( 'dropdown-css' );
  $( '.dropdown-act' ).live( 'click', function( ev ) {
   ev.preventDefault();
  } );
  $( '.dropdown' ).live( 'focus', function( ev ) {
   R.fun.console( 'fun.dropdown - focus: opened' );
   R.vars.dropdown_selected = $( this ).hasClass( 'selected' );
   $( this ).addClass( 'selected' );
  } ).live( 'blur', function( ev ) {
   R.fun.console( 'fun.dropdown - blur: closed' );
   $( '.dropdown' ).removeClass( 'selected' );
  } ).live( 'click', function( ev ) {
   if( R.vars.dropdown_selected ) {
    R.fun.console( 'fun.dropdown - click: closed' );
    $( '.dropdown' ).removeClass( 'selected' );
    R.vars.dropdown_selected = false;
   }
   else {
    R.fun.console( 'fun.dropdown - click: opened' );
    $( '.dropdown' ).removeClass( 'selected' );
    $( this ).addClass( 'selected' );
    R.vars.dropdown_selected = true;
   }
  } );
 }
};

// Attiva l'interfaccia a tab (supporta solo 2 tab), ed è navigabile anche mediante tastiera
// tab: selettore in formato jQuery
// current: vale 0 o 1, è l'indice del tab da mostrare al caricamento (0 di default)
R.fun.dualtab = function( tab, current ) {
 if( R.fun.cssEnabled() ) {
  current = R.fun.isset( current ) ? current : 0;
  $( '.tab', $( tab ) ).each( function( index, element ) {
   $( '.titolo h1', $( element ) ).wrapInner( $( '<a>', { href: '#', 'data-href': '#tab-' + index } ) );
   $( '.contenuto', $( element ) ).attr( { 'data-id': 'tab-' + index } );
  } );
  $( tab ).css( { position: 'relative' } );
  $( '.tab .titolo:last', $( tab ) ).css( { position: 'absolute', right: '0px', top: '0px', 'width': '47%' } );
  $( '.tab .titolo:last h1', $( tab ) ).css( { float: 'right', 'margin-top': '0px', 'width': '100%' } );
  $( '.tab .titolo h1', $( tab ) ).css( { 'background-color': '#eee', 'border-top-color': '#ddd' } );
  $( '.tab .titolo h1', $( tab ) ).eq( current ).css( { 'background-color': '#fff', 'border-top-color': '#f3cb50' } );
  $( '.contenuto', $( tab ) ).css( { position: 'absolute', left: '0px', top: '-100000px' } );
  $( '.contenuto', $( tab ) ).eq( current ).css( { position: 'static', 'margin-top': '0px' } );
  $( '.tab .titolo', $( tab ) ).css( { 'cursor': 'pointer' } );

  // Fix 30/06/2014 - Corretto il click sui link interni al tab
  $( '.tab .titolo', $( tab ) ).live( 'click focus', function( ev ) {
   ev.preventDefault();
   var id = $( 'h1 a', this ).attr( 'data-href' ).replace( /^#/g, '' );
   R.fun.console( 'fun.dualtab - click/focus: ' + id );
   $( '.tab .titolo h1', $( tab ) ).css( { 'background-color': '#eee', 'border-top-color': '#ddd' } );
   $( 'h1', this ).css( { 'background-color': '#fff', 'border-top-color': '#f3cb50' } );
   $( '.contenuto', $( tab ) ).css( { position: 'absolute' } );
   $( '.contenuto[data-id=' + id + ']', $( tab ) ).css( { position: 'static' } );

   // IE7 workaround - refresh di immagini sul cambio tab
   setTimeout( function() {
    $( '.contenuto[data-id=' + id + '] img', $( tab ) ).each( function() { $( this ).addClass( 'ie7' ); } );
   }, 1 );

  } );
  // Fix 30/06/2014: fine

 }
};

// Assegna una o più classi CSS ad un a selezione di elementi della pagina in caso di evento
// selector: selettore in formato jQuery sul quale attivare l'evento
// classes: classe/i da assegnare al selettore in caso di evento
// event_in: evento in entrata (mouseover di default)
// event_out: evento in uscita (mouseout di default)
R.fun.toggleClass = function( selector, classes, event_in, event_out ) {
 if( R.fun.cssEnabled() ) {
  event_in = event_in ? event_in : 'mouseover';
  event_out = event_out ? event_out : 'mouseout';
  $( selector ).live( event_in, function( ev) {
   ev.preventDefault();
   $( this ).addClass( classes );
  } ).live( event_out, function( ev ) {
   ev.preventDefault();
   $( this ).removeClass( classes );
  } );
 }
};

// Assegna/rimuove in alternanza una classe CSS ad una selezione di elementi della pagina in caso di evento
// selector: selettore in formato jQuery sul quale attivare l'evento
// class_: classe da assegnare al selettore in caso di evento
// event: evento in entrata e in uscita (click di default)
R.fun.toggleEvent = function( selector, class_, event ) {
 if( R.fun.cssEnabled() ) {
  event = R.fun.isset( event ) ? event : 'click';
  $( selector ).live( event, function( ev ) {
   ev.preventDefault();
   var has = $( this ).hasClass( class_ );
   $( selector + '.' + class_ ).removeClass( class_ );

   if( ! has )
    $( this ).addClass( class_ );
  } );
 }
};

// Determina se i css sono abilitati
R.fun.cssEnabled = function() {
 $( 'body' ).append( '<div id="cssEnabled"></div>' );
 $( '#cssEnabled' ).css( { position: 'absolute', top: '-10000px', left: '-10000px' } );
 var test = $( '#cssEnabled' ).position().top < 0;
 $( '#cssEnabled' ).remove();
 R.fun.console( 'un.cssEnabled - ' + test );
 return test;
};

// Verifica se una variabile javascript è definita e valorizzata
// v: la variabile
R.fun.isset = function( v ) {
 return ( typeof v !== "undefined" ) && v;
};

// Scrive un testo nella console
// txt: il testo da scrivere
R.fun.console = function( txt ) {
 if( R.vars.debug && R.fun.isset( console ) && R.fun.isset( console.log ) )
  console.log( txt );
};

// Eventi di inizializzazione
$( document ).ready( R.ev.ready );
$( window ).load( R.ev.load );
$( window ).resize( R.ev.resize );

//Multi tab Pagine Servizi
$(document).ready(function(){
	$("#tab2").hide();
	$(".tab_servizio").click(function() {
		$($(".tab_servizio.current a").attr("href")).hide();
		$(".tab_servizio.current").removeClass("current");
	
		$($(this).find("a").attr("href")).show();
		$(this).addClass("current");
	
		return false;
	});
});

