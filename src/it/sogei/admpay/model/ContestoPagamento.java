package it.sogei.admpay.model;

import java.io.Serializable;



public class ContestoPagamento implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nomeCategoria ;
	private int idCategoria ;
	private String areaCategoria;
	private String styleSelected;
	private boolean pagamentoSpontaneo;
	private String denominazioneServizio;

	
	
	
	public String getNomeCategoria() {
		return nomeCategoria;
	}
	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}
	
	public int getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getAreaCategoria() {
		return areaCategoria;
	}
	public void setAreaCategoria(String areaCategoria) {
		this.areaCategoria = areaCategoria;
	}
	public String getStyleSelected() {
		return styleSelected;
	}
	public void setStyleSelected(String styleSelected) {
		this.styleSelected = styleSelected;
	}
	public boolean isPagamentoSpontaneo() {
		return pagamentoSpontaneo;
	}
	public void setPagamentoSpontaneo(boolean pagamentoSpontaneo) {
		this.pagamentoSpontaneo = pagamentoSpontaneo;
	}
	public String getDenominazioneServizio() {
		return denominazioneServizio;
	}
	public void setDenominazioneServizio(String denominazioneServizio) {
		this.denominazioneServizio = denominazioneServizio;
	}
	 
		

}
