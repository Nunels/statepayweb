package it.sogei.admpay.model;

import java.io.Serializable;

public class DominioPagoPA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String iuv;
	private String codiceContesto;
	private String dominio;
	public String getIuv() {
		return iuv;
	}
	public void setIuv(String iuv) {
		this.iuv = iuv;
	}
	public String getCodiceContesto() {
		return codiceContesto;
	}
	public void setCodiceContesto(String codiceContesto) {
		this.codiceContesto = codiceContesto;
	}
	public String getDominio() {
		return dominio;
	}
	public void setDominio(String dominio) {
		this.dominio = dominio;
	}
	
	

}
