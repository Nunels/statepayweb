package it.sogei.admpay.model;

import java.io.Serializable;

public class Utente  implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cfPagatore;
	private String emailPagatore;
	private String cfVersante;
	private String emailVersante;
	private Long idServizio;
	private String denominazione;
	private String provinciaPagatore = "CS";
	
	public String getCfPagatore() {
		return cfPagatore;
	}
	public void setCfPagatore(String cfPagatore) {
		this.cfPagatore = cfPagatore;
	}
	public String getEmailPagatore() {
		return emailPagatore;
	}
	public void setEmailPagatore(String emailPagatore) {
		this.emailPagatore = emailPagatore;
	}
	public String getCfVersante() {
		return cfVersante;
	}
	public void setCfVersante(String cfVersante) {
		this.cfVersante = cfVersante;
	}
	public String getEmailVersante() {
		return emailVersante;
	}
	public void setEmailVersante(String emailVersante) {
		this.emailVersante = emailVersante;
	}
	public Long getIdServizio() {
		return idServizio;
	}
	public void setIdServizio(Long idServizio) {
		this.idServizio = idServizio;
	}
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getProvinciaPagatore() {
		return provinciaPagatore;
	}
	public void setProvinciaPagatore(String provinciaPagatore) {
		this.provinciaPagatore = provinciaPagatore;
	}
	
	

}
