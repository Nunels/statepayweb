package it.sogei.admpay.model;

import java.io.Serializable;

public class DatiUtente implements Serializable {
	
	private String denominazione = null;
	private String siglaProvincia = null;
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getSiglaProvincia() {
		return siglaProvincia;
	}
	public void setSiglaProvincia(String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}
	@Override
	public String toString() {
		return "DatiUtente [denominazione=" + denominazione
				+ ", siglaProvincia=" + siglaProvincia + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((denominazione == null) ? 0 : denominazione.hashCode());
		result = prime * result
				+ ((siglaProvincia == null) ? 0 : siglaProvincia.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatiUtente other = (DatiUtente) obj;
		if (denominazione == null) {
			if (other.denominazione != null)
				return false;
		} else if (!denominazione.equals(other.denominazione))
			return false;
		if (siglaProvincia == null) {
			if (other.siglaProvincia != null)
				return false;
		} else if (!siglaProvincia.equals(other.siglaProvincia))
			return false;
		return true;
	}
	

}
