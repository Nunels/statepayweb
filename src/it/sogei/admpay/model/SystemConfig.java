package it.sogei.admpay.model;

import java.io.Serializable;

public class SystemConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String ambienteSuCuiGiro = null;
	
	
	private boolean wssec = false;
	private String endPointNodo = null;
	
	private String hostServiceNodo = null;
	
	private String hostServiceNodoDownload=null;
	
	private String pathRestServiceNodo = null;
	
	private String certPath = null;
	private String certPwd = null;
	private String certType = null;
	
	private boolean npadIsEnable =  false;
	private String npadHost = null;
	private String npadPath = null;
	private String nomeServizio = null;
	private String codiceAutorizzazione = null;

	private String endpointAssistenza = null;
	
	
	private String endPointAudmHost = null;
	private String endPointAudmPath = null;

	private boolean tls12Esclusivo = false;
	
	private String hostService = null;
	
	private String ADMPayRServiceApi = null;
	private String ADMPayReceiverRServiceApi = null;
	private String ADMPayCartRServiceApi = null;
	private String ADMPayRTRServiceApi = null;

	private String headerKey1=null;
	private String headerValue1=null;
	private String headerKey2=null;
	private String headerValue2=null;
	
	private boolean generatorDSEnable=false;
	
	private boolean audmIsEnable =  false;
	private String  audmIstance = null;
	
	
	private String	tipoAgenzia = null;
	private String	system = null;
	private String	component = null;
	
	private boolean utenteDiTest = false;
	
	private String expRegUserMatch = null; 
	
	
	public String getTipoAgenzia() {
		return tipoAgenzia;
	}

	public void setTipoAgenzia(String tipoAgenzia) {
		this.tipoAgenzia = tipoAgenzia;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public boolean isWssec() {
		return wssec;
	}

	public void setWssec(boolean wssec) {
		this.wssec = wssec;
	}

	public String getEndPointNodo() {
		return endPointNodo;
	}

	public void setEndPointNodo(String endPointNodo) {
		this.endPointNodo = endPointNodo;
	}
	
	public String getHostServiceNodo() {
		return hostServiceNodo;
	}

	public void setHostServiceNodo(String hostServiceNodo) {
		this.hostServiceNodo = hostServiceNodo;
	}

	public String getCertPath() {
		return certPath;
	}

	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	public String getCertPwd() {
		return certPwd;
	}

	public void setCertPwd(String certPwd) {
		this.certPwd = certPwd;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getNpadHost() {
		return npadHost;
	}

	public void setNpadHost(String npadHost) {
		this.npadHost = npadHost;
	}

	public String getNpadPath() {
		return npadPath;
	}

	public void setNpadPath(String npadPath) {
		this.npadPath = npadPath;
	}

	public String getNomeServizio() {
		return nomeServizio;
	}

	public void setNomeServizio(String nomeServizio) {
		this.nomeServizio = nomeServizio;
	}

	public String getCodiceAutorizzazione() {
		return codiceAutorizzazione;
	}

	public void setCodiceAutorizzazione(String codiceAutorizzazione) {
		this.codiceAutorizzazione = codiceAutorizzazione;
	}

	public String getEndpointAssistenza() {
		return endpointAssistenza;
	}

	public void setEndpointAssistenza(String endpointAssistenza) {
		this.endpointAssistenza = endpointAssistenza;
	}

	public String getEndPointAudmHost() {
		return endPointAudmHost;
	}

	public void setEndPointAudmHost(String endPointAudmHost) {
		this.endPointAudmHost = endPointAudmHost;
	}

	public String getEndPointAudmPath() {
		return endPointAudmPath;
	}

	public void setEndPointAudmPath(String endPointAudmPath) {
		this.endPointAudmPath = endPointAudmPath;
	}

	public boolean isTls12Esclusivo() {
		return tls12Esclusivo;
	}

	public void setTls12Esclusivo(boolean tls12Esclusivo) {
		this.tls12Esclusivo = tls12Esclusivo;
	}

	public String getHostService() {
		return hostService;
	}

	public void setHostService(String hostService) {
		this.hostService = hostService;
	}

	public String getADMPayRServiceApi() {
		return ADMPayRServiceApi;
	}

	public void setADMPayRServiceApi(String aDMPayRServiceApi) {
		ADMPayRServiceApi = aDMPayRServiceApi;
	}

	public String getADMPayRTRServiceApi() {
		return ADMPayRTRServiceApi;
	}

	public void setADMPayRTRServiceApi(String aDMPayRTRServiceApi) {
		ADMPayRTRServiceApi = aDMPayRTRServiceApi;
	}

	public String getADMPayReceiverRServiceApi() {
		return ADMPayReceiverRServiceApi;
	}

	public void setADMPayReceiverRServiceApi(String aDMPayReceiverRServiceApi) {
		ADMPayReceiverRServiceApi = aDMPayReceiverRServiceApi;
	}

	public String getADMPayCartRServiceApi() {
		return ADMPayCartRServiceApi;
	}

	public void setADMPayCartRServiceApi(String aDMPayCartRServiceApi) {
		ADMPayCartRServiceApi = aDMPayCartRServiceApi;
	}

	public boolean isAudmIsEnable() {
		return audmIsEnable;
	}

	public void setAudmIsEnable(boolean audmIsEnable) {
		this.audmIsEnable = audmIsEnable;
	}

	public String getAudmIstance() {
		return audmIstance;
	}

	public void setAudmIstance(String audmIstance) {
		this.audmIstance = audmIstance;
	}

	public boolean isNpadIsEnable() {
		return npadIsEnable;
	}

	public void setNpadIsEnable(boolean npadIsEnable) {
		this.npadIsEnable = npadIsEnable;
	}

	public boolean isGeneratorDSEnable() {
		return generatorDSEnable;
	}

	public void setGeneratorDSEnable(boolean generatorDSEnable) {
		this.generatorDSEnable = generatorDSEnable;
	}

	public String getAmbienteSuCuiGiro() {
		return ambienteSuCuiGiro;
	}

	public void setAmbienteSuCuiGiro(String ambienteSuCuiGiro) {
		this.ambienteSuCuiGiro = ambienteSuCuiGiro;
	}

	public boolean isUtenteDiTest() {
		return utenteDiTest;
	}

	public void setUtenteDiTest(boolean utenteDiTest) {
		this.utenteDiTest = utenteDiTest;
	}

	public String getHeaderKey1() {
		return headerKey1;
	}

	public void setHeaderKey1(String headerKey1) {
		this.headerKey1 = headerKey1;
	}

	public String getHeaderValue1() {
		return headerValue1;
	}

	public void setHeaderValue1(String headerValue1) {
		this.headerValue1 = headerValue1;
	}

	public String getHeaderKey2() {
		return headerKey2;
	}

	public void setHeaderKey2(String headerKey2) {
		this.headerKey2 = headerKey2;
	}

	public String getHeaderValue2() {
		return headerValue2;
	}

	public void setHeaderValue2(String headerValue2) {
		this.headerValue2 = headerValue2;
	}



	public String getPathRestServiceNodo() {
		return pathRestServiceNodo;
	}

	public void setPathRestServiceNodo(String pathRestServiceNodo) {
		this.pathRestServiceNodo = pathRestServiceNodo;
	}

	public String getExpRegUserMatch() {
		return expRegUserMatch;
	}

	public void setExpRegUserMatch(String expRegUserMatch) {
		this.expRegUserMatch = expRegUserMatch;
	}

	public String getHostServiceNodoDownload() {
		return hostServiceNodoDownload;
	}

	public void setHostServiceNodoDownload(String hostServiceNodoDownload) {
		this.hostServiceNodoDownload = hostServiceNodoDownload;
	}

}
