package it.sogei.admpay.service;

import gov.telematici.pagamenti.ws.join.ASelezionaRPT;
import it.gov.digitpa.schemas._2011.pagamenti.CtRichiestaPagamentoTelematico;
import it.sogei.admpay.api.ADMPayRServiceApiTest;
import it.sogei.admpay.client.Service;
import it.sogei.admpay.model.AdmPay;
import it.sogei.admpay.model.AdmPayData;
import it.sogei.admpay.model.AdmPayFlusso;
import it.sogei.admpay.model.AdmPayList;
import it.sogei.admpay.model.AdmPayRT;
import it.sogei.admpay.model.AdmPayRTList;
import it.sogei.admpay.model.Dominio;
import it.sogei.admpay.service.setup.MenuService;
import it.sogei.data.DataUtilFactory;
import it.sogei.pay.system.model.external.FiltroFlusso;
import it.sogei.pay.system.model.external.Flusso;
import it.sogei.pay.system.model.external.FlussoData;
import it.sogei.ssi.service.app.UserService;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.AppUtil;
import it.sogei.ssi.util.DateUtil;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.model.SelectItem;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.Document;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;

@ManagedBean
@ViewScoped
public class StatoPagamentiService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(StatoPagamentiService.class);

	private ADMPayRServiceApiTest api = new ADMPayRServiceApiTest();

	private List<Flusso> listaPendenze = null;

	private LazyDataModel<Flusso> listaPendenzeLazy = null;

	private CtRichiestaPagamentoTelematico richiesta = null;

	private Long idRichiesta = null;

	private Service service = null;

	private Dominio dominio = null;

	private AdmPayList admPayList = new AdmPayList();

	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{menuService}")
	private MenuService menuService;

	@ManagedProperty("#{datiSpecifici}")
	private DatiSpecifici datiSpecifici;

	private Object ricercaObject;
	private boolean ricercaByKey;

	private PhaseId phase = null;

	private FlussoData flussoData = new FlussoData();

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	PrimeFaces instance = PrimeFaces.current();

	// dominio
	private String content;
	private String layout;

	private Map<String, String> ec = null;
	private List<SelectItem> listItem = null;
	private String selectedEC;
	private long activeIndexSogei;

	//
	private String pagatore = "";
	private String versante = "";
	private Date dataRichiesta = null;
	private String codContesto = "";
	private String iuv = "";

	//

	public StatoPagamentiService() {
		if (log == null) {
			// PropertyConfigurator.configure(AppConf.getLogConfFile());
			log = Logger.getLogger(StatoPagamentiService.class);

		}
		listItem = AppConf.tuttiIdomini();
		activeIndexSogei = AppConf.getActiveIndexSogei(listItem);

		layout = AppConf.getDominio().getLayout();
		content = AppConf.getDominio().getContent();
		AppUtil.setInSessionMap("selectedEC", AppConf.getDominio()
				.getIdDominio());
		selectedEC = AppConf.getDominio().getIdDominio();

	}

	@PostConstruct
	private void init() {
		log.info("> start init()");
		dominio = AppConf.getDominio();

		service = new Service();

		caricaPendenze();

		log.info("> end init()");
	}

	public void cerca() {
		listaPendenze = null;
		listaPendenzeLazy = null;

		init();
	}

	public void caricaPendenze() {

		log.info("> start caricaPendenze");
		/*
		 * if(FacesContext.getCurrentInstance().getCurrentPhaseId().equals(phase)
		 * && userService.getUtenteCorrente().equals(utente)) return;
		 */
		phase = FacesContext.getCurrentInstance().getCurrentPhaseId();

		log.info("Utente: " + userService.getUtenteCorrente());

		listaPendenzeLazy = new LazyDataModel<Flusso>() {
			@Override
			public List<Flusso> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				if (AppUtil.getCurrentInstance().isValidationFailed()
						|| menuService.isEmptyForm()) {
					return flussoData.getFlussoList();
				}

				listaPendenze = new ArrayList<Flusso>();
				Object dRichiesta = filters.get("dataRichiesta");
				Object dRicevuta = filters.get("dataRicevuta");
				filters.remove("dataRichiesta");
				filters.remove("dataRicevuta");

				FiltroFlusso filtro = new FiltroFlusso();

				Flusso flussoFilter = new Flusso();
				if (filters != null && filters.size() > 0) {

					try {
						BeanUtils.populate(flussoFilter,
								new HashMap<String, Object>(filters));
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

				}

				if (dRichiesta != null) {
					// log.info((Date)dRichiesta);
					flussoFilter.setDataRichiesta(DateUtil
							.toXmlGregorianCalendar((Date) dRichiesta));
				} else {
					flussoFilter.setDataRichiesta(null);
				}

				if (dRicevuta != null) {
					// log.info((Date)dRicevuta);
					flussoFilter.setDataRicevuta(DateUtil
							.toXmlGregorianCalendar((Date) dRicevuta));
				} else {
					flussoFilter.setDataRicevuta(null);
				}

				//
				// filtri ricerca
				//

				if (pagatore != null && !pagatore.equals("")) {
					flussoFilter.setIdPagatore(pagatore);
				} else {
					flussoFilter.setIdPagatore(null);
				}

				if (versante != null && !versante.equals("")) {
					flussoFilter.setIdVersante(versante);

				} else {
					flussoFilter.setIdVersante(null);
				}

				if (dataRichiesta != null) {
					// log.info((Date)dRichiesta);
					flussoFilter.setDataRichiesta(DateUtil
							.toXmlGregorianCalendar(dataRichiesta));
				} else {
					flussoFilter.setDataRichiesta(null);
				}

				if (iuv != null) {
					// log.info((Date)dRichiesta);
					flussoFilter.setIuv(iuv);
				} else {
					flussoFilter.setIuv(null);
				}
				if (codContesto != null) {
					// log.info((Date)dRichiesta);
					flussoFilter.setCodiceContesto(codContesto);
				} else {
					flussoFilter.setCodiceContesto(null);
				}
				//
				//
				//

				if (ricercaByKey && ricercaObject != null) {
					first = 0;
					AdmPayData admPayData = new AdmPayData();

					AdmPayFlusso admPayFlusso = new AdmPayFlusso();

					AdmPay admPay = new AdmPay();

					admPay.setDominio(dominio.getIdDominio());
					admPay.setCfIntestatario(userService.getUtenteCorrente());
					admPay.setCfUtente(userService.getUtenteCorrente());

					if (menuService.getSelectedCategoria() != null) {
						admPay.setSiglaServizio(menuService
								.getSelectedCategoria().getNomeCategoria());
						admPay.setIdServizio(menuService.getSelectedCategoria()
								.getIdCategoria());
						admPay.setArea(menuService.getSelectedCategoria()
								.getAreaCategoria());

						// instance.current().executeScript("PF('statoPagamentoTable').paginator.setPage(0);");
					}

					admPay.setDatiSpecifici(DataUtilFactory
							.trasformObjectToJsonString(ricercaObject));

					if (datiSpecifici.getAdmPayKeyFilter() != null) {
						admPay.setAdmPayKeyFilter(datiSpecifici
								.getAdmPayKeyFilter());
						datiSpecifici.setAdmPayKeyFilter(null);
					}

					admPayFlusso.setDa(first + 1);
					admPayFlusso.setA(first + pageSize);

					admPayFlusso.setAdmPay(admPay);

					admPayData.setAdmPayFlusso(admPayFlusso);

					log.info("Ricerca AdmPaysByKey JSON:"
							+ new String(DataUtilFactory
									.trasformObjectToJsonString(admPayData)));

					admPayData = api.selectAdmPaysByKey(contentTypeJson,
							acceptLanguage, admPayData);

					if (admPayData.getAdmPayFlusso().getId() != null
							&& !admPayData.getAdmPayFlusso().getId().isEmpty()) {

						AdmPayRT admPayRT = new AdmPayRT();

						admPayRT.setCfIntestatario(userService
								.getUtenteCorrente());
						admPayRT.setCfUtente(userService.getUtenteCorrente());

						admPayRT.setId(admPayData.getAdmPayFlusso().getId()
								.get(0));

						log.info("Ricerca AdmPayRTByID JSON:"
								+ new String(DataUtilFactory
										.trasformObjectToJsonString(admPayRT)));

						// AdmPayRTList
						// admPayRTList=api.selectAdmPayRTByID(contentTypeJson,
						// acceptLanguage, admPayRT);

						AdmPayRTList admPayRTList = new AdmPayRTList();

						if (admPayRTList != null
								&& admPayRTList.getAdmPayRT() != null
								&& !admPayRTList.getAdmPayRT().isEmpty()) {

							AdmPayRT admRt = admPayRTList.getAdmPayRT().get(0);

							flussoFilter.setIuv(admRt.getIuv());
							flussoFilter.setCodiceContesto(admRt
									.getCodiceContesto());

						}
					}

				}

				if (sortField != null) {
					boolean sort = true;
					switch (sortField) {
					case "iuv":
						sortField = "IUV";
						break;
					case "codiceContesto":
						sortField = "CODICE_CONTESTO";
						break;
					case "idDominio":
						sortField = "ID_DOMINIO";
						break;
					case "statoPagamento":
						sortField = "STATO_PAGAMENTO";
						break;
					case "esitoPagamento":
						sortField = "ESITO_PAGAMENTO";
						break;
					case "idPagatore":
						sortField = "ID_PAGATORE";
						break;
					case "idVersante":
						sortField = "ID_VERSANTE";
						break;
					case "dataRicevuta":
						sortField = "DATA_RICEVUTA";
						break;
					case "dataRichiesta":
						sortField = "DATA_RICHIESTA";
						break;
					default:
						sort = false;
						break;
					}
					if (sort) {
						filtro.setOrderBy(sortField);
						if (sortOrder == SortOrder.ASCENDING) {
							filtro.setOrderType("ASC");
						} else if (sortOrder == SortOrder.DESCENDING) {
							filtro.setOrderType("DESC");
						}
					}
				}

				filtro.setDa(first + 1);
				filtro.setA(first + pageSize);

				if (flussoFilter != null
						&& flussoFilter.getStatoPagamento() != null) {

					log.info("Stato pagamento: "
							+ flussoFilter.getStatoPagamento());
					flussoFilter.setStatoPagamento(flussoFilter
							.getStatoPagamento().replaceAll(" ", "_"));

				}

				if (flussoFilter != null
						&& flussoFilter.getEsitoPagamento() != null) {

					log.info("Esito pagamento: "
							+ flussoFilter.getEsitoPagamento());
					flussoFilter.setEsitoPagamento(flussoFilter
							.getEsitoPagamento().replaceAll(" ", "_"));

				}

				// flussoFilter.setIdPagatore(userService.getUtenteCorrente());

				if (menuService != null
						&& menuService.getSelectedCategoria() != null) {
					flussoFilter.setIdServizio(new Long(menuService
							.getSelectedCategoria().getIdCategoria()));
				}

				flussoFilter.setIdDominio(dominio.getIdDominio());

				filtro.setFlusso(flussoFilter);

				flussoData = new FlussoData();
				flussoData.setFiltroFlusso(filtro);

				try {
					// flussoData = service.nodoClient()
					// .selezionaPagamentiInCorso(flussoData);
				} catch (Exception e) {
					log.error(e, e);
					AppUtil.getCurrentInstance().addMessage(
							"statoGenerale",
							new FacesMessage(FacesMessage.SEVERITY_FATAL,
									"Errore di sistema:",
									"Riprovare in un altro momento"));
				}

				listaPendenzeLazy.setRowCount(flussoData.getFiltroFlusso()
						.getCount());

				listaPendenze = flussoData.getFlussoList();

				return listaPendenze;

			}

		};

		log.info("< end caricaPendenze()");

	}

	/*
	 * public void aggiornaStato(Flusso flusso, Integer index){
	 * 
	 * 
	 * 
	 * AStatoRPTCopiaRT stato = null;
	 * 
	 * try { NodoChiediStatoRPT chiediStatoRPT = new NodoChiediStatoRPT();
	 * 
	 * 
	 * chiediStatoRPT.setCodiceContestoPagamento(flusso.getCodiceContesto());
	 * chiediStatoRPT.setIdentificativoDominio(flusso.getIdDominio());
	 * chiediStatoRPT
	 * .setIdentificativoIntermediarioPA(dominio.getIdIntermediarioPA());
	 * chiediStatoRPT.setIdentificativoStazioneIntermediarioPA(dominio.
	 * getIdStazioneIntermediarioPA());
	 * chiediStatoRPT.setIdentificativoUnivocoVersamento(flusso.getIuv());
	 * chiediStatoRPT.setPassword(dominio.getPwdPA());
	 * 
	 * 
	 * 
	 * stato = service.nodoClient().nodoChiediStatoRPT(chiediStatoRPT);//header,
	 * dominio.getPwdPA());
	 * 
	 * 
	 * if(stato!= null && stato.getFaultBean()==null &&
	 * stato.getNodoChiediCopiaRTRisposta()!=null){ NodoChiediCopiaRTRisposta
	 * ricevuta = stato.getNodoChiediCopiaRTRisposta();
	 * 
	 * 
	 * 
	 * 
	 * if (ricevuta.getFault()==null){ aggiornaPendenze();
	 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_INFO, "Info:",
	 * "Rivecuta aquisita, � possibile consultare l'esito nel pannello dello storico"
	 * ));
	 * 
	 * }else{ AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:",
	 * "Errore nel recupero della ricevuta")); }
	 * 
	 * 
	 * 
	 * }else if(stato!=null && stato.getFaultBean()==null){ aggiornaPendenze();
	 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Stato aggiornato"));
	 * 
	 * }
	 * 
	 * 
	 * } catch (Exception e) { log.error(e,e);
	 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:",
	 * "Riprovare in un altro momento")); }
	 * 
	 * 
	 * 
	 * 
	 * }
	 * 
	 * public void chiediRicevuta(Flusso flusso, Integer index){
	 * 
	 * 
	 * 
	 * NodoChiediCopiaRTRisposta ricevuta = null;
	 * 
	 * try {
	 * 
	 * NodoChiediCopiaRT chiediCopiaRT = new NodoChiediCopiaRT();
	 * chiediCopiaRT.setCodiceContestoPagamento(flusso.getCodiceContesto());
	 * chiediCopiaRT.setIdentificativoDominio(flusso.getIdDominio());
	 * chiediCopiaRT
	 * .setIdentificativoIntermediarioPA(dominio.getIdIntermediarioPA());
	 * chiediCopiaRT.setIdentificativoStazioneIntermediarioPA(dominio.
	 * getIdStazioneIntermediarioPA());
	 * chiediCopiaRT.setIdentificativoUnivocoVersamento(flusso.getIuv());
	 * chiediCopiaRT.setPassword( dominio.getPwdPA());
	 * 
	 * ricevuta =
	 * service.nodoClient().nodoChiediCopiaRT(chiediCopiaRT);//header,
	 * dominio.getPwdPA());
	 * 
	 * 
	 * if (ricevuta.getFault()==null){ aggiornaPendenze();
	 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_INFO, "Info:",
	 * "Rivecuta aquisita, � possibile consultare l'esito nel pannello dello storico"
	 * )); }else{ AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore:",
	 * "Errore nel recupero della ricevuta")); }
	 * 
	 * } catch (Exception e) { log.error(e,e);
	 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
	 * FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:",
	 * "Riprovare in un altro momento")); } }
	 */

	public void aggiorna() {
		log.info("> start aggiornaPendenze");
		listaPendenze = null;
		menuService.setEmptyForm(false);
		// listaPendenzeLazy = null;
		log.info("Stato aggiorna");
		caricaPendenze();

		log.info("< end aggiornaPendenze");
	}

	public void viewDettaglio(Flusso flusso) {

		AdmPayRT admPayRT = new AdmPayRT();
		admPayRT.setCodiceContesto(flusso.getCodiceContesto());
		admPayRT.setIdDominio(flusso.getIdDominio());
		admPayRT.setIuv(flusso.getIuv());
		admPayRT.setCfIntestatario(flusso.getIdPagatore());

		admPayList = api.selectAdmPayByIdPagoPA(contentTypeJson,
				acceptLanguage, admPayRT);
	}

	public List<Flusso> getListaPendenze() {

		// caricaPendenze();

		return listaPendenze;
	}

	public void setListaPendenze(List<Flusso> listaPendenze) {
		this.listaPendenze = listaPendenze;
	}

	public void preProcessPDF(Object document) {
		Document doc = (Document) document;

		doc.setPageSize(PageSize.A4.rotate());

		HeaderFooter footer = new HeaderFooter(new Phrase(""), true);
		doc.setFooter(footer);

	}

	public void onRowToggle(ToggleEvent event) {
		// log.info("Row State " + event.getVisibility());
		Flusso flusso = ((Flusso) event.getData());
		// log.info( "Flusso:" + ((Flusso) event.getData()));

		if (event.getVisibility().toString().equals("VISIBLE")
				&& ((richiesta != null && idRichiesta != null && !idRichiesta
						.equals(flusso.getIdRichiesta())) || richiesta == null)) {

			try {

				ASelezionaRPT aSelezionaRPT = new ASelezionaRPT();

				aSelezionaRPT.setCfPagatore(flusso.getIdPagatore());
				aSelezionaRPT.setIdRichiesta(flusso.getIdRichiesta());

				richiesta = service.nodoClient().selezionaRPT(aSelezionaRPT);// flusso.getIdRichiesta(),flusso.getIdPagatore());
				idRichiesta = flusso.getIdRichiesta();

			} catch (Exception e) {
				log.error(e, e);
				AppUtil.getCurrentInstance().addMessage(
						"statoGenerale",
						new FacesMessage(FacesMessage.SEVERITY_FATAL,
								"Errore di sistema:",
								"Riprovare in un altro momento"));

			}
		}

	}

	public CtRichiestaPagamentoTelematico getRichiesta() {
		return richiesta;
	}

	public void setRichiesta(CtRichiestaPagamentoTelematico richiesta) {
		this.richiesta = richiesta;
	}

	public LazyDataModel<Flusso> getListaPendenzeLazy() {
		return listaPendenzeLazy;
	}

	public void setListaPendenzeLazy(LazyDataModel<Flusso> listaPendenzeLazy) {
		this.listaPendenzeLazy = listaPendenzeLazy;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public DatiSpecifici getDatiSpecifici() {
		return datiSpecifici;
	}

	public void setDatiSpecifici(DatiSpecifici datiSpecifici) {
		this.datiSpecifici = datiSpecifici;
	}

	public Object getRicercaObject() {
		return ricercaObject;
	}

	public void setRicercaObject(Object ricercaObject) {
		this.ricercaObject = ricercaObject;
	}

	public boolean isRicercaByKey() {
		return ricercaByKey;
	}

	public void setRicercaByKey(boolean ricercaByKey) {
		this.ricercaByKey = ricercaByKey;
	}

	public void ricerca() {
		menuService.setEmptyForm(false);
		ricercaObject = datiSpecifici.getRicercaObject();
		ricercaByKey = true;
		listaPendenzeLazy = null;
		init();
	}

	public void verificaNotEmpty() {
		menuService.setEmptyForm(false);
		if (datiSpecifici.getDominioPagoPA() == null
				|| (datiSpecifici.getDominioPagoPA().getIuv().isEmpty() && datiSpecifici
						.getDominioPagoPA().getCodiceContesto().isEmpty())) {
			AppUtil.getCurrentInstance().addMessage(
					"pa",
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Attenzione!",
							"Compilare almeno un campo di ricerca"));
			menuService.setEmptyForm(true);
		}
	}

	public AdmPayList getAdmPayList() {
		return admPayList;
	}

	public void setAdmPayList(AdmPayList admPayList) {
		this.admPayList = admPayList;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getSelectedEC() {
		return selectedEC;
	}

	public void setSelectedEC(String selectedEC) {
		this.selectedEC = selectedEC;
	}

	public long getActiveIndexSogei() {
		return activeIndexSogei;
	}

	public void setActiveIndexSogei(long activeIndexSogei) {
		this.activeIndexSogei = activeIndexSogei;
	}

	public Long getIdRichiesta() {
		return idRichiesta;
	}

	public void setIdRichiesta(Long idRichiesta) {
		this.idRichiesta = idRichiesta;
	}

	public Dominio getDominio() {
		return dominio;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public Map<String, String> getEc() {
		return ec;
	}

	public void setEc(Map<String, String> ec) {
		this.ec = ec;
	}

	public String getPagatore() {
		return pagatore;
	}

	public void setPagatore(String pagatore) {
		this.pagatore = pagatore;
	}

	public String getVersante() {
		return versante;
	}

	public void setVersante(String versante) {
		this.versante = versante;
	}

	public Date getdRichiesta() {
		return dataRichiesta;
	}

	public void setdRichiesta(Date dRichiesta) {
		this.dataRichiesta = dRichiesta;
	}

	public String getCodContesto() {
		return codContesto;
	}

	public void setCodContesto(String codContesto) {
		this.codContesto = codContesto;
	}

	public String getIuv() {
		return iuv;
	}

	public void setIuv(String iuv) {
		this.iuv = iuv;
	}

}
