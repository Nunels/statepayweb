package it.sogei.admpay.service;

import gov.telematici.pagamenti.ws.join.ASelezionaCarrello;
import gov.telematici.pagamenti.ws.join.ASelezionaRPT;
import gov.telematici.pagamenti.ws.join.ASelezionaRT;
import gov.telematici.pagamenti.ws.ppthead.IntestazioneCarrelloPPT;
import it.gov.digitpa.schemas._2011.pagamenti.CtRichiestaPagamentoTelematico;
import it.sogei.admpay.api.ADMPayRServiceApiTest;
import it.sogei.admpay.client.Service;
import it.sogei.admpay.model.AdmPay;
import it.sogei.admpay.model.AdmPayData;
import it.sogei.admpay.model.AdmPayFlusso;
import it.sogei.admpay.model.AdmPayRT;
import it.sogei.admpay.model.AdmPayRTList;
import it.sogei.admpay.model.Dominio;
import it.sogei.admpay.service.setup.MenuService;
import it.sogei.data.DataUtilFactory;
import it.sogei.pay.system.model.external.FiltroFlusso;
import it.sogei.pay.system.model.external.Flusso;
import it.sogei.pay.system.model.external.FlussoData;
import it.sogei.pay.system.state.SogeiPaySystemException;
import it.sogei.ssi.service.app.UserService;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.AppUtil;
import it.sogei.ssi.util.DateUtil;
import it.sogei.ssi.util.exporter.RawBytesExporter;
import it.sogei.util.rest.client.Response;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.Document;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;


@ManagedBean
@ViewScoped
public class StoricoPagamentiService implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6609076186196171930L;
	
	
	private static Logger log = Logger.getLogger(StoricoPagamentiService.class);

	
	private List<Flusso> listaStoricoPagamenti  = null;
	
	private LazyDataModel<Flusso> listaStoricoPagamentiLazy = null;
		
	private CtRichiestaPagamentoTelematico richiesta = null;
    
	private ADMPayRServiceApiTest api=new ADMPayRServiceApiTest();
	
	//private HashMap<Flusso, EsitoQuietanza> mapQuietanza = null;
	
	//private HashMap<Flusso, byte[]> mapRicevute = null;
	
	private FlussoData flussoData=new FlussoData();
	
	private Service service = null;
	
	private Dominio dominio=null;
	
	private PhaseId phase =null;
	
	@ManagedProperty("#{userService}")
	private UserService userService;
	
	@ManagedProperty("#{menuService}")
	private MenuService menuService;
	

	@ManagedProperty("#{datiSpecifici}")
	private DatiSpecifici datiSpecifici;
	
	private Object ricercaObject;
	private boolean ricercaByKey;
	
	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";
	
	private Integer size= null;
	
	PrimeFaces instance = PrimeFaces.current();
	
//	private Map<String, Integer> rowSpanMap = null;
//	private Set<String> opzioniComuniRender =null;
	
	@PostConstruct
	private void init(){
		log.info("> start init()");		
		dominio = AppConf.getDominio();
		
		
		service = new Service();
		
		//utente = userService.getUtenteCorrente();

		
		caricaStoricoPagamenti();
		
	   log.info("< end init()");
	}
	
	public void caricaStoricoPagamenti(){
		log.info("> start caricaStoricoPagamenti()");
	//	log.info("phase "+FacesContext.getCurrentInstance().getCurrentPhaseId());
	/*	if(FacesContext.getCurrentInstance().getCurrentPhaseId().equals(phase) && userService.getUtenteCorrente().equals(utente))
			return;
		*/
		phase = FacesContext.getCurrentInstance().getCurrentPhaseId();
		
		//utente = userService.getUtenteCorrente();
		log.info(userService.getUtenteCorrente()+" utente");
		
		listaStoricoPagamentiLazy = new LazyDataModel<Flusso>() {
			@Override
			public List<Flusso> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
				
				if(AppUtil.getCurrentInstance().isValidationFailed()||menuService.isEmptyForm()){
					return flussoData.getFlussoList();
				}
				
				listaStoricoPagamenti = new ArrayList<Flusso>();
				Object dRichiesta = filters.get("dataRichiesta");
				Object dRicevuta = filters.get("dataRicevuta");
				filters.remove("dataRichiesta");
				filters.remove("dataRicevuta");
				
				FiltroFlusso filtro = new FiltroFlusso();
				
				Flusso  flussoFilter = new Flusso();
				if(filters!=null && filters.size()>0){
					
					try {
						BeanUtils.populate(flussoFilter, new HashMap<String, Object>(filters));
					} catch (IllegalAccessException | InvocationTargetException e) {e.printStackTrace();}
		
				}
				
			
				if(dRichiesta  != null){
					//log.info((Date)dRichiesta);
					flussoFilter.setDataRichiesta(DateUtil.toXmlGregorianCalendar((Date)dRichiesta));
				}else{
					flussoFilter.setDataRichiesta(null);
				}
				
				if(dRicevuta!= null){
					//log.info((Date)dRicevuta);
					flussoFilter.setDataRicevuta(DateUtil.toXmlGregorianCalendar((Date)dRicevuta));
				}else{
					flussoFilter.setDataRicevuta(null);
				}
				
				if(ricercaByKey&&ricercaObject!=null){
					first = 0;
					AdmPayData admPayData=new AdmPayData();
					
					AdmPayFlusso admPayFlusso = new AdmPayFlusso();
					
					AdmPay admPay=new AdmPay();
					
					admPay.setDominio(dominio.getIdDominio());
					admPay.setCfIntestatario(userService.getUtenteCorrente());
					admPay.setCfUtente(userService.getUtenteCorrente());
					
					if(menuService.getSelectedCategoria()!=null){
						admPay.setSiglaServizio(menuService.getSelectedCategoria().getNomeCategoria());
						admPay.setIdServizio(menuService.getSelectedCategoria().getIdCategoria());
						admPay.setArea(menuService.getSelectedCategoria().getAreaCategoria());
					//	 instance.current().executeScript("PF('storicoPagamentiTable').paginator.setPage(0);"); 
					}
					
					admPay.setDatiSpecifici(DataUtilFactory.trasformObjectToJsonString(ricercaObject));
					
					if(datiSpecifici.getAdmPayKeyFilter()!=null){
						admPay.setAdmPayKeyFilter(datiSpecifici.getAdmPayKeyFilter());
						datiSpecifici.setAdmPayKeyFilter(null);
					}
					
					admPayFlusso.setDa(first+1);
					admPayFlusso.setA(first+pageSize);
				
					admPayFlusso.setAdmPay(admPay);
					
					admPayData.setAdmPayFlusso(admPayFlusso);
					
					log.info("Ricerca AdmPaysByKey JSON:"+new String(DataUtilFactory.trasformObjectToJsonString(admPayData)));
					
					admPayData=api.selectAdmPaysByKey(contentTypeJson, acceptLanguage, admPayData);
					
					if(admPayData.getAdmPayFlusso().getId()!=null&&!admPayData.getAdmPayFlusso().getId().isEmpty()){
						
						AdmPayRT admPayRT=new AdmPayRT();
						
						
						admPayRT.setCfIntestatario(userService.getUtenteCorrente());
						admPayRT.setCfUtente(userService.getUtenteCorrente());
						
						admPayRT.setId(admPayData.getAdmPayFlusso().getId().get(0));
						
						log.info("Ricerca AdmPayRTByID JSON:"+new String(DataUtilFactory.trasformObjectToJsonString(admPayRT)));
						
						AdmPayRTList admPayRTList=api.selectAdmPayRTByID(contentTypeJson, acceptLanguage, admPayRT);
						
						if(admPayRTList!=null&&admPayRTList.getAdmPayRT()!=null&&!admPayRTList.getAdmPayRT().isEmpty()){
							
							AdmPayRT admRt=admPayRTList.getAdmPayRT().get(0);
							
							flussoFilter.setIuv(admRt.getIuv());
							flussoFilter.setCodiceContesto(admRt.getCodiceContesto());
							
						}
					}
					
					
					
				}

				
				if (sortField != null ) {
					boolean sort =true;
					switch (sortField) {
					case "iuv":
						sortField = "IUV";
						break;
					case "codiceContesto":
						sortField = "CODICE_CONTESTO";
						break;
					case "idDominio":
						sortField = "ID_DOMINIO";
						break;
					case "statoPagamento":
						sortField = "STATO_PAGAMENTO";
						break;
					case "esitoPagamento":
						sortField = "ESITO_PAGAMENTO";
						break;
					case "idPagatore":
						sortField = "ID_PAGATORE";
						break;
					case "idVersante":
						sortField = "ID_VERSANTE";
						break;
					case "dataRicevuta":
						sortField = "DATA_RICEVUTA";
						break;
					case "dataRichiesta":
						sortField = "DATA_RICHIESTA";
						break;
					default:
						sort = false;
						break;
					}			
					if(sort){
						filtro.setOrderBy(sortField);
			            if (sortOrder == SortOrder.ASCENDING) {
			            	filtro.setOrderType("ASC");
			            } else if (sortOrder == SortOrder.DESCENDING) {
			            	filtro.setOrderType("DESC");
			            }
					}
		        }
				
				if(flussoFilter!=null &&  flussoFilter.getStatoPagamento()!=null){
					
					log.info("Stato pagamento: "+flussoFilter.getStatoPagamento());
					flussoFilter.setStatoPagamento(flussoFilter.getStatoPagamento().replaceAll(" ", "_"));
					
					
				}
				
				if(flussoFilter!=null &&  flussoFilter.getEsitoPagamento()!=null){
					
					log.info("Esito pagamento: "+flussoFilter.getEsitoPagamento());
					flussoFilter.setEsitoPagamento(flussoFilter.getEsitoPagamento().replaceAll(" ", "_"));
					
				}
				
				
				 
				filtro.setDa(first+1);
				filtro.setA(first+pageSize);
				
				size = pageSize;
				
				log.info("utente "+userService.getUtenteCorrente());
				flussoFilter.setIdPagatore(userService.getUtenteCorrente());
				if(menuService!= null && menuService.getSelectedCategoria()!=null){
					flussoFilter.setIdServizio(new Long(menuService.getSelectedCategoria().getIdCategoria()));
					log.info("utente "+userService.getUtenteCorrente() +" id servizio "+menuService.getSelectedCategoria().getIdCategoria());
				}
				
				
				flussoFilter.setIdDominio(dominio.getIdDominio());
				
			
				filtro.setFlusso(flussoFilter);
				
				
				flussoData= new FlussoData();
				flussoData.setFiltroFlusso(filtro);
				
				try {
					flussoData = service.nodoClient().selezionaStoricoPagamenti(flussoData);
				}catch (Exception e) {
					AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
				}
	
				listaStoricoPagamentiLazy.setRowCount(flussoData.getFiltroFlusso().getCount());
				
				
				listaStoricoPagamenti =	flussoData.getFlussoList();
				
//				for (Flusso f : flussoData.getFlussoList()){
//					log.info(f);
//					
//					if(f.isSetMarcaDaBollo()){
//						for(MarcaDaBollo m : f.getMarcaDaBollo()){
//							log.info("MaRCA");
//							log.info(m);
//						}
//					}
//				}
				
			
				
				return listaStoricoPagamenti;
				
			}

		};
		
		log.info("< end caricaStoricoPagamenti");
		
	}
	
	/* public boolean renderOpzioniComuni(String idCarrello){
		log.info("ret "+idCarrello);
		
		if(idCarrello==null || idCarrello.equals("")){
			return false;
		}

		boolean ret = true;
		
		if(opzioniComuniRender==null){
			opzioniComuniRender = new TreeSet<String>();
			opzioniComuniRender.add(idCarrello);
		}else{
			if(opzioniComuniRender.contains(idCarrello)){
				ret = false;
			}else{
				opzioniComuniRender.add(idCarrello);
			}
		}
		log.info(idCarrello + " ll - ll "+ret);
		
		return ret;
	}
	
	 
	
	public int rowSpan(String idCarrello){
		
		
		
		if(rowSpanMap==null){
			rowSpanMap = new HashMap<String, Integer>();
			
			for(Flusso f : listaStoricoPagamenti){
				if(f.getIdCarrello()!=null){
				int i = 0;
				if(rowSpanMap.containsKey(f.getIdCarrello())){
					i = rowSpanMap.get(f.getIdCarrello());
					i=i+2;
				}else{
					i =2;
				}
				log.info(f.getIdCarrello()+ " aa-aa  "+i);
				
				rowSpanMap.put(f.getIdCarrello(), i);
				
				}
			}
		}
		int ret = 2;
		if(rowSpanMap.containsKey(idCarrello)){
			ret =rowSpanMap.get(idCarrello);
			//ret = ret*2;	
		}
	
		return ret ;
		
	}*/
	
	
	public void aggiorna(){
		listaStoricoPagamenti = null;
		menuService.setEmptyForm(false);
		//rowSpanMap = null;
		//opzioniComuniRender = null;
		//listaStoricoPagamentiLazy = null;
		log.info("Storico aggiorna");
		caricaStoricoPagamenti();
		
	
	}
	

	public void verificaNotEmpty(){
		menuService.setEmptyForm(false);
		if(datiSpecifici.getDominioPagoPA()==null||
				(datiSpecifici.getDominioPagoPA().getIuv().isEmpty()&&datiSpecifici.getDominioPagoPA().getCodiceContesto().isEmpty())){
			AppUtil.getCurrentInstance().addMessage("pa", new FacesMessage(FacesMessage.SEVERITY_WARN, "Attenzione!", "Compilare almeno un campo di ricerca"));
			menuService.setEmptyForm(true);
		}
	}
	
	public void onRowToggle(ToggleEvent event) {
		
	   Flusso flusso = ((Flusso) event.getData());
	
		   
	   if(event.getVisibility().toString().equals("VISIBLE")){
	
		   
	
	
		   if(flusso.getIdRichiesta()!=null && flusso.getIdRichiesta()>0){
			  
			   try {
				   ASelezionaRPT aSelezionaRPT = new ASelezionaRPT();
				   aSelezionaRPT.setCfPagatore(flusso.getIdPagatore());
				   aSelezionaRPT.setIdRichiesta(flusso.getIdRichiesta());
				   
				   richiesta = service.nodoClient().selezionaRPT(aSelezionaRPT);//flusso.getIdRichiesta(), flusso.getIdPagatore());
				  
			  
				  
			   } catch (SogeiPaySystemException e) {
				   
				   if(e instanceof SogeiPaySystemException ){
		     			
		     		//	log.info(((SogeiPaySystemException) e).getDescrizione() +" "+((SogeiPaySystemException) e).getStatoPA()+" "+((SogeiPaySystemException) e).getTipoException());
		     			
//		     			if(((SogeiPaySystemException) e).getErrori()!=null){
//			     			List<Errore> el = ((SogeiPaySystemException) e).getErrori().getErrore();
//				     		for (Errore errore : el) {
//				     			log.info(errore.getCodice()+" "+errore.getDescrizione());	
//							
//				     		}
//		     			}
		     		}
				   
				   log.error(e,e);
		     	   AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
					
			   }
		   }
	   }
	  
	}
	
	
	
	
	
	
	public void downloadRicevuta(Integer index){
		
		log.info("> start downloadRicevuta()");
		// per paginazione riporto l'indice in pagina
		index = index %size;
		
		
		Flusso flusso  = listaStoricoPagamenti.get(index);
	
		
		byte[] xml = null;

		try{
			/*if(mapRicevute==null){
				mapRicevute = new HashMap<Flusso, byte[]>();
			}
			
			if((xml =mapRicevute.get(flusso))==null){*/
							
				
				ASelezionaRT rtFirmata = new ASelezionaRT();
				rtFirmata.setCfPagatore( flusso.getIdPagatore());
				rtFirmata.setIdRicevuta(flusso.getIdRicevuta());
				xml = service.nodoClient().selezionaRTFirmataBytes(rtFirmata).getBodyBytes();//selezionaRTFirmata(flusso.getIdRicevuta(), flusso.getIdPagatore());
				
			/*	if(xml!=null){
					mapRicevute.put(flusso, xml);
				}
			}*/
			
			
			if(xml!=null && xml.length>0){
				
				
				RawBytesExporter exporter = new RawBytesExporter(flusso.getIdDominio()+"_"+flusso.getCodiceContesto()+"_"+flusso.getIuv()+".xml");
				exporter.setContentType("application/xml");
				exporter.setByteArray(xml);
				exporter.export();
				
			}else{

				AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Download Ricevuta non disponibile"));
			
			}	
			
	        
		}catch(Exception e){
			log.error(e,e);
	     	AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
		}
	   log.info("< end downloadRicevuta()"); 
		
	}

	public void downloadAttestazioneRT(Integer index){
		log.info("> start downloadAttestazioneRT()");
		// per paginazione riporto l'indice in pagina
		index = index %size;
		
		
		Flusso flusso  = listaStoricoPagamenti.get(index);
	
		
		byte[] xml = null;

		try{
			/*if(mapRicevute==null){
				mapRicevute = new HashMap<Flusso, byte[]>();
			}
			
			if((xml =mapRicevute.get(flusso))==null){*/
							
				
				ASelezionaRT rtFirmata = new ASelezionaRT();
				rtFirmata.setCfPagatore( flusso.getIdPagatore());
				rtFirmata.setIdRicevuta(flusso.getIdRicevuta());
				xml = service.nodoClient().selezionaAttestazioneRT(rtFirmata).getBodyBytes();//selezionaRTFirmata(flusso.getIdRicevuta(), flusso.getIdPagatore());
				
			/*	if(xml!=null){
					mapRicevute.put(flusso, xml);
				}
			}*/
			
			
			if(xml!=null && xml.length>0){
				
				
				RawBytesExporter exporter = new RawBytesExporter(flusso.getIdDominio()+"_"+flusso.getCodiceContesto()+"_"+flusso.getIuv()+".pdf");
				exporter.setContentType("application/pdf");
				exporter.setByteArray(xml);
				exporter.export();
				
			}else{

				AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Download Ricevuta non disponibile"));
			
			}	
			
	        
		}catch(Exception e){
			log.error(e,e);
	     	AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
		}
	   log.info("< end downloadAttestazioneRT");     
	}
	
	
	public void downloadCarrelloRicevuta(Integer index){
		log.info("> start downloadCarrelloRicevuta");
		// per paginazione riporto l'indice in pagina
		index = index %size;
	
		Flusso flusso  = listaStoricoPagamenti.get(index);
		
		int countInCarrello =0;
		int countRicevute = 0;
		
		for (int i =0; i<listaStoricoPagamenti.size(); i++){
			Flusso flussoDaControllare = listaStoricoPagamenti.get(i); 
			if(flussoDaControllare.getIdCarrello()!= null && flussoDaControllare.getIdCarrello().equals(flusso.getIdCarrello())){
				countInCarrello++;
				if(flussoDaControllare.getIdRicevuta()!=null && flussoDaControllare.getIdRicevuta()> 0l){
					countRicevute++;
				}
			}
		}
		
		if(countInCarrello != countRicevute){
			AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Ricevuta multipla non ancora disponibile"));
			return;
		}

	
		
		byte[] zip = null;

		try{
			Response r   = null;
			/*if(mapRicevute==null){
				mapRicevute = new HashMap<Flusso, byte[]>();
			}
			
			
			if((zip =mapRicevute.get(flusso))==null){*/
							
				ASelezionaCarrello aSelezionaCarrello = new ASelezionaCarrello();
				IntestazioneCarrelloPPT ic = new IntestazioneCarrelloPPT();
				
				ic.setIdentificativoCarrello(flusso.getIdCarrello());
				ic.setIdentificativoIntermediarioPA(dominio.getIdIntermediarioPA());
				ic.setIdentificativoStazioneIntermediarioPA(dominio.getIdStazioneIntermediarioPA());
				
				
				aSelezionaCarrello.setIntestazioneCarrelloPPT(ic);
				
				
				
				r =service.nodoClient().selezionaRTFirmataDaCarrelloBytes(aSelezionaCarrello);
				zip = r.getBodyBytes();
				
				/*if(zip!=null){
					mapRicevute.put(flusso, zip);
				}
			
			}*/
			
			
			if(zip!=null && zip.length>0){
				
				
				RawBytesExporter exporter = new RawBytesExporter("RicevutaPagamento_"+flusso.getIdCarrello()+".zip");
				exporter.setContentType("application/octet-stream");
				exporter.setByteArray(zip);
				exporter.export();
				
			}else{

				AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Download Ricevuta non disponibile"));
			
			}	
			
	        
		}catch(Exception e){
			log.error(e,e);
	     	AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
		}
	   log.info("< end downloadCarrelloRicevuta");     
	}
	
	
	public void downloadAttestazioneCarrello(Integer index){
		log.info("> start downloadAttestazioneCarrello");
		// per paginazione riporto l'indice in pagina
		index = index %size;
	
		Flusso flusso  = listaStoricoPagamenti.get(index);
		
		int countInCarrello =0;
		int countRicevute = 0;
		
		for (int i =0; i<listaStoricoPagamenti.size(); i++){
			Flusso flussoDaControllare = listaStoricoPagamenti.get(i); 
			if(flussoDaControllare.getIdCarrello()!= null && flussoDaControllare.getIdCarrello().equals(flusso.getIdCarrello())){
				countInCarrello++;
				if(flussoDaControllare.getIdRicevuta()!=null && flussoDaControllare.getIdRicevuta()> 0l){
					countRicevute++;
				}
			}
		}
		
		if(countInCarrello != countRicevute){
			AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Ricevuta multipla non ancora disponibile"));
			return;
		}

	
		
		byte[] zip = null;

		try{
			Response r   = null;
			/*if(mapRicevute==null){
				mapRicevute = new HashMap<Flusso, byte[]>();
			}
			
			
			if((zip =mapRicevute.get(flusso))==null){*/
							
				ASelezionaCarrello aSelezionaCarrello = new ASelezionaCarrello();
				IntestazioneCarrelloPPT ic = new IntestazioneCarrelloPPT();
				
				ic.setIdentificativoCarrello(flusso.getIdCarrello());
				ic.setIdentificativoIntermediarioPA(dominio.getIdIntermediarioPA());
				ic.setIdentificativoStazioneIntermediarioPA(dominio.getIdStazioneIntermediarioPA());
				
				
				aSelezionaCarrello.setIntestazioneCarrelloPPT(ic);
				
				
				
				r =service.nodoClient().selezionaAttestazioneRTDaCarrello(aSelezionaCarrello);
				zip = r.getBodyBytes();
				
				/*if(zip!=null){
					mapRicevute.put(flusso, zip);
				}
			
			}*/
			
			
			if(zip!=null && zip.length>0){
				
				
				RawBytesExporter exporter = new RawBytesExporter("AttestazionePagamento_"+flusso.getIdCarrello()+".pdf");
				exporter.setContentType("application/pdf");
				exporter.setByteArray(zip);
				exporter.export();
				
			}else{

				AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Download Ricevuta non disponibile"));
			
			}	
			
	        
		}catch(Exception e){
			log.error(e,e);
	     	AppUtil.getCurrentInstance().addMessage("statoGenerale", new FacesMessage(FacesMessage.SEVERITY_FATAL, "Errore di sistema:", "Riprovare in un altro momento"));
		}
	    log.info("< end downloadAttestazioneCarrello");    
	}
	

	public List<Flusso> getListaStoricoPagamenti() {
	//	caricaStoricoPagamenti();
		return listaStoricoPagamenti;
	}

	public void setListaStoricoPagamenti(List<Flusso> listaStoricoPagamenti) {
		this.listaStoricoPagamenti = listaStoricoPagamenti;
	}
	
	
	
	public CtRichiestaPagamentoTelematico getRichiesta() {
		return richiesta;
	}

	public void setRichiesta(CtRichiestaPagamentoTelematico richiesta) {
		this.richiesta = richiesta;
	}
	
	public LazyDataModel<Flusso> getListaStoricoPagamentiLazy() {
		return listaStoricoPagamentiLazy;
	}

	public void setListaStoricoPagamentiLazy( LazyDataModel<Flusso> listaStoricoPagamentiLazy) {
		this.listaStoricoPagamentiLazy = listaStoricoPagamentiLazy;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}



	
	public void preProcessPDF(Object document) {
	    Document doc = (Document) document;
	    
	    doc.setPageSize(PageSize.A4.rotate());
	    
	    
	    HeaderFooter footer = new HeaderFooter(new Phrase(""), true);
	    doc.setFooter(footer);
	    
	}
	
	public void ricerca(){
		ricercaObject = datiSpecifici.getRicercaObject();
		ricercaByKey=true;	
		listaStoricoPagamentiLazy = null;
		menuService.setEmptyForm(false);
		init();
	}


	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public DatiSpecifici getDatiSpecifici() {
		return datiSpecifici;
	}

	public void setDatiSpecifici(DatiSpecifici datiSpecifici) {
		this.datiSpecifici = datiSpecifici;
	}

	public Object getRicercaObject() {
		return ricercaObject;
	}

	public void setRicercaObject(Object ricercaObject) {
		this.ricercaObject = ricercaObject;
	}

	public boolean isRicercaByKey() {
		return ricercaByKey;
	}

	public void setRicercaByKey(boolean ricercaByKey) {
		this.ricercaByKey = ricercaByKey;
	}

	
}
