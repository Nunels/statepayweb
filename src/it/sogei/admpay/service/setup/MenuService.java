package it.sogei.admpay.service.setup;

import it.sogei.admpay.api.ADMPayRServiceApiTest;
import it.sogei.admpay.model.ContestoPagamento;
import it.sogei.admpay.service.DatiSpecifici;
import it.sogei.pay.system.model.external.servizi.Servizi;
import it.sogei.pay.system.model.external.servizi.ServiziData;
import it.sogei.ssi.util.AppUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class MenuService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int activeIndex = 0;
	private String currentPageCategorie ;
	private String currentPageDebiti ;
	
	private boolean bolloDigitale=false ;
	
	private ContestoPagamento selectedCategoria;
	 
	private int activeStep=0;
	
	private ServiziData serviziData=new ServiziData();
	
	private List<ContestoPagamento> contestiPagamento;
	
	private ContestoPagamento contestoPagamento;
	
	private ADMPayRServiceApiTest admServiceApiTest=new ADMPayRServiceApiTest();
	private Map<String, List<ContestoPagamento>> mapAreaCategorie=new HashMap<String, List<ContestoPagamento>>();
	
	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";
	
	private static final String unSelectColor="";
	private static final String selectColor="-selected";
	
	public static final String areaDogane="DOGANE";
	public static final String areaAccise="ACCISE";
	public static final String areaMonopoli="MONOPOLI";
	
	
	public static final String allAree="ADM";
	
	private String coloreAreaDogane;
	private String coloreAreaAccise;
	private String coloreAreaMonopoli;
	
	@ManagedProperty("#{datiSpecifici}")
	private DatiSpecifici datiSpecifici;
	
	private boolean emptyForm=false;
	
	public MenuService() {
		
		serviziData=admServiceApiTest.servicesList(contentTypeJson, acceptLanguage);
		
		mapAreaCategorie.put(allAree, new ArrayList<ContestoPagamento>());
		if(mapAreaCategorie.get(areaAccise)==null){
			mapAreaCategorie.put(areaAccise, new ArrayList<ContestoPagamento>());
		}
		if(mapAreaCategorie.get(areaDogane)==null){
			mapAreaCategorie.put(areaDogane, new ArrayList<ContestoPagamento>());
		}
		if(mapAreaCategorie.get(areaMonopoli)==null){
			mapAreaCategorie.put(areaMonopoli, new ArrayList<ContestoPagamento>());
		}
		if(mapAreaCategorie.get(allAree)==null){
			mapAreaCategorie.put(allAree, new ArrayList<ContestoPagamento>());
		}
		 
		for(Servizi serviziObj:serviziData.getServiziList()){
			
			if(mapAreaCategorie.get(serviziObj.getArea())==null){
				mapAreaCategorie.put(serviziObj.getArea(), new ArrayList<ContestoPagamento>());
				
			}
		  
			ContestoPagamento categoria=new ContestoPagamento();
			categoria.setIdCategoria(serviziObj.getIdServizio());
			categoria.setNomeCategoria(serviziObj.getServizio());
			categoria.setAreaCategoria(serviziObj.getArea());
			categoria.setStyleSelected(unSelectColor);
			categoria.setDenominazioneServizio(serviziObj.getDenominazioneServizio());
			
			if(serviziObj.getArea().equals(allAree)){
				
				
			
				
				categoria.setAreaCategoria(areaAccise);
				mapAreaCategorie.get(areaAccise).add(categoria);
								
				categoria.setAreaCategoria(areaDogane);
				mapAreaCategorie.get(areaDogane).add(categoria);
				
				categoria.setAreaCategoria(areaMonopoli);
				mapAreaCategorie.get(areaMonopoli).add(categoria);
				
				categoria.setAreaCategoria(allAree);

			}else{
			
				mapAreaCategorie.get(serviziObj.getArea()).add(categoria);
			}
			mapAreaCategorie.get(allAree).add(categoria);
			
			
			
		}
		
		contestiPagamento = mapAreaCategorie.get(allAree);
		
		currentPageCategorie=Page.categorie;
		currentPageDebiti=Page.pagamentiInCorso;
		
		resetColorAree();
	}

	public int getActiveIndex() {
		return activeIndex;
	}
	
	public void setActiveIndex(Long activeIndex){
		
		int categoria=activeIndex.intValue();
		
		currentPageCategorie = Page.categorie;
		
	    if(activeStep==0){
		  currentPageDebiti=Page.pagamentiInCorso;
		}
		
		resetColorAree();
		
		if(contestiPagamento!=null){
		  resetColorCategorie();
		}
		
		switch (categoria) {
		case 0:
			contestiPagamento=mapAreaCategorie.get(areaDogane);
			coloreAreaDogane=selectColor;
			break;	
		case 1:
			contestiPagamento=mapAreaCategorie.get(areaAccise);
			coloreAreaAccise=selectColor;
			break;
		case 2:
			contestiPagamento=mapAreaCategorie.get(areaMonopoli);
			coloreAreaMonopoli=selectColor;
		    break;
		 
	   
		default:
			break;
		}
		
	
		this.activeIndex = activeIndex.intValue();
	}

	public int getActiveStep() {
		return activeStep;
	}

	public void setActiveStep(int activeStep) {
		this.activeStep = activeStep;
	}

	private void resetColorCategorie(){
	
	  selectedCategoria= null;
	  
	  for(ContestoPagamento cat:contestiPagamento){
		  cat.setStyleSelected(unSelectColor);
	  }
	}
	
	private void resetColorAree(){
	
		coloreAreaAccise=unSelectColor;
		coloreAreaDogane=unSelectColor;
		coloreAreaMonopoli=unSelectColor;
	}
	
	private void setColoreArea(String area){
		if(area.equals(allAree)){
			
		}else if(area.equals(areaAccise)){
			coloreAreaAccise=selectColor;
		}else if(area.equals(areaDogane)){
			coloreAreaDogane=selectColor;
		}else if(area.equals(areaMonopoli)){
			coloreAreaMonopoli=selectColor;
		}
	}
	
	public Map<String, List<ContestoPagamento>> getMapAreaCategorie() {
		return mapAreaCategorie;
	}

	public void setMapAreaCategorie(Map<String, List<ContestoPagamento>> mapAreaCategorie) {
		this.mapAreaCategorie = mapAreaCategorie;
	}

	public void getListaElement(ContestoPagamento categoria){
		
		if(AppUtil.getCurrentInstance().getMessages().hasNext()){
			AppUtil.getCurrentInstance().getMessages().remove();
		}
		
		emptyForm=false;
		
		if(activeStep==0){
		  this.currentPageDebiti=Page.pagamentiInCorso;
		}else if(activeStep==1){
			this.currentPageDebiti=Page.storicoPagamenti;
		}
		
		selectedCategoria = categoria;
		
		
		if(!selectedCategoria.getAreaCategoria().equals(allAree)){
			
			resetColorAree();
		
			setColoreArea(categoria.getAreaCategoria());
		}
		
		datiSpecifici.reset();
		
		
		for(int i=0;i<contestiPagamento.size();i++){
			if(contestiPagamento.get(i).getNomeCategoria().equals(categoria.getNomeCategoria())){
				contestiPagamento.get(i).setStyleSelected(selectColor);
				
			}else{
				contestiPagamento.get(i).setStyleSelected(unSelectColor);
			}
		}
		
		
	}
	
	public void changeStep(long value) {
		activeStep=(int)value;
		
		selectedCategoria = null;
		emptyForm=false;
		datiSpecifici.reset();
		
		switch (activeStep) {
		
		case 0:
			currentPageCategorie=Page.categorie;
			currentPageDebiti=Page.pagamentiInCorso;
			break;
		case 1:
			currentPageCategorie=Page.categorie;
			currentPageDebiti=Page.storicoPagamenti;
			break;
		
		default:
			break;
		}
		
		
		if(contestiPagamento!=null){
			resetColorCategorie();
		}
		
		resetColorAree();
    }
	
	
	public List<ContestoPagamento> getCategorie() {
		return contestiPagamento;
	}

	public void setCategorie(List<ContestoPagamento> categorie) {
		this.contestiPagamento = categorie;
	}

	
	public String getCurrentPageCategorie() {
		return currentPageCategorie;
	}

	public void setCurrentPageCategorie(String currentPageCategorie) {
		this.currentPageCategorie = currentPageCategorie;
	}

	public String getCurrentPageDebiti() {
		return currentPageDebiti;
	}

	public void setCurrentPageDebiti(String currentPageDebiti) {
		this.currentPageDebiti = currentPageDebiti;
	}

	public String getColoreAreaDogane() {
		return coloreAreaDogane;
	}

	public void setColoreAreaDogane(String coloreAreaDogane) {
		this.coloreAreaDogane = coloreAreaDogane;
	}

	public String getColoreAreaAccise() {
		return coloreAreaAccise;
	}

	public void setColoreAreaAccise(String coloreAreaAccise) {
		this.coloreAreaAccise = coloreAreaAccise;
	}

	public String getColoreAreaMonopoli() {
		return coloreAreaMonopoli;
	}

	public void setColoreAreaMonopoli(String coloreAreaMonopoli) {
		this.coloreAreaMonopoli = coloreAreaMonopoli;
	}


	public ContestoPagamento getContestoPagamento() {
		return contestoPagamento;
	}

	public void setContestoPagamento(ContestoPagamento contestoPagamento) {
		this.contestoPagamento = contestoPagamento;
	}

	public ContestoPagamento getSelectedCategoria() {
		return selectedCategoria;
	}

	public void setSelectedCategoria(ContestoPagamento selectedCategoria) {
		this.selectedCategoria = selectedCategoria;
	}

	public DatiSpecifici getDatiSpecifici() {
		return datiSpecifici;
	}

	public void setDatiSpecifici(DatiSpecifici datiSpecifici) {
		this.datiSpecifici = datiSpecifici;
	}

	public boolean isBolloDigitale() {
		return bolloDigitale;
	}

	public void setBolloDigitale(boolean bolloDigitale) {
		this.bolloDigitale = bolloDigitale;
	}

	public boolean isEmptyForm() {
		return emptyForm;
	}

	public void setEmptyForm(boolean emptyForm) {
		this.emptyForm = emptyForm;
	}
	
}
