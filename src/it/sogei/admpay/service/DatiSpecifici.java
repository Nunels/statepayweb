package it.sogei.admpay.service;

import it.sogei.admpay.model.AdmPay;
import it.sogei.admpay.model.AdmPayCart;
import it.sogei.admpay.model.AdmPayKeyFilter;
import it.sogei.admpay.model.DominioPagoPA;
import it.sogei.admpay.model.bollodigitale.BolloDigitale;
import it.sogei.admpay.model.contodidebito.Opccd;
import it.sogei.admpay.model.dichiarazione.DichiarazioneDoganale;
import it.sogei.admpay.model.saldi.giochi.Saldi;
import it.sogei.data.DataUtilFactory;
import it.sogei.ssi.util.AdmPayUtil;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;

@ManagedBean
@SessionScoped
public class DatiSpecifici implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date dataCorrente = DateUtil.getDate();

	private Date dataRicerca;

	private AdmPayKeyFilter admPayKeyFilter = null;

	private DominioPagoPA dominioPagoPA = new DominioPagoPA();
	private Opccd opccd = new Opccd();
	private DichiarazioneDoganale docpa = new DichiarazioneDoganale();
	private BolloDigitale bolloDigitale = new BolloDigitale();
	private Saldi saldi = new Saldi();

	private Object ricercaObject = null;

	private String pagatore = "";
	private String versante = "";

	private static Logger log = Logger.getLogger(DatiSpecifici.class);

	public Object getRicercaObject() {
		return ricercaObject;
	}

	public void setRicercaObject(Object ricercaObject) {
		this.ricercaObject = ricercaObject;
	}

	public DominioPagoPA getDominioPagoPA() {
		dominioPagoPA.setDominio(AppConf.getDominio().getIdDominio());
		return dominioPagoPA;
	}

	public void setDominioPagoPA(DominioPagoPA dominioPagoPA) {
		this.dominioPagoPA = dominioPagoPA;
	}

	public Opccd getOpccd() {
		opccd.setPfIiniziale("CD-");
		opccd.setPfIntermedia("-P-");
		opccd.setPfFinale("-");

		return opccd;
	}

	public void setOpccd(Opccd opccd) {
		this.opccd = opccd;
	}

	public DichiarazioneDoganale getDocpa() {
		if (dataRicerca != null) {
			/*
			 * Calendar cal = Calendar.getInstance(); cal.setTime(dataRicerca);
			 * 
			 * log.info("Current Year is : " + cal.get(Calendar.YEAR)); // month
			 * start from 0 to 11 log.info("Current Month is : " +
			 * (cal.get(Calendar.MONTH) + 1)); log.info("Current Day is : "
			 * +cal.get(Calendar.DAY_OF_MONTH));
			 */

			docpa.setDataReg(DateUtil
					.toXMLGregorianCalendarWithoutTimeStamp(dataRicerca));

			/*
			 * admPayKeyFilter = new AdmPayKeyFilter(); MapFilter filter = new
			 * MapFilter(); filter.setKey("dataReg >= #date#");
			 * filter.setValue(dataRicerca);
			 * admPayKeyFilter.getMapFilter().add(filter);
			 */

		}
		return docpa;
	}

	public void setDocpa(DichiarazioneDoganale docpa) {
		this.docpa = docpa;
	}

	public Date getDataCorrente() {
		return dataCorrente;
	}

	public void setDataCorrente(Date dataCorrente) {
		this.dataCorrente = dataCorrente;
	}

	public Date getDataRicerca() {
		return dataRicerca;
	}

	public void setDataRicerca(Date dataRicerca) {
		this.dataRicerca = dataRicerca;
	}

	public <T> T getDatiSpecifici(Object obj) throws ClassNotFoundException {

		AdmPay admPay = new AdmPay();

		if (obj instanceof AdmPay) {
			admPay = (AdmPay) obj;
		} else if (obj instanceof AdmPayCart) {
			AdmPayCart admPayCart = (AdmPayCart) obj;
			admPay = admPayCart.getAdmPay();
		}

		return (T) DataUtilFactory.populateObjectFromJson(
				AdmPayUtil.getClassName(admPay), admPay.getDatiSpecifici());

	}

	public void reset() {
		ricercaObject = null;
		dataRicerca = null;

		opccd = new Opccd();

		docpa = new DichiarazioneDoganale();

		bolloDigitale = new BolloDigitale();
	}

	public AdmPayKeyFilter getAdmPayKeyFilter() {
		return admPayKeyFilter;
	}

	public void setAdmPayKeyFilter(AdmPayKeyFilter admPayKeyFilter) {
		this.admPayKeyFilter = admPayKeyFilter;
	}

	public BolloDigitale getBolloDigitale() {

		if ("".equals(bolloDigitale.getProtocollo()))
			bolloDigitale.setProtocollo(null);
		if ("".equals(bolloDigitale.getIdPay()))
			bolloDigitale.setIdPay(null);

		return bolloDigitale;
	}

	public void setBolloDigitale(BolloDigitale bolloDigitale) {
		this.bolloDigitale = bolloDigitale;
	}

	public Saldi getSaldi() {
		return saldi;
	}

	public void setSaldi(Saldi saldi) {
		this.saldi = saldi;
	}

	public String getPagatore() {
		return pagatore;
	}

	public void setPagatore(String pagatore) {
		this.pagatore = pagatore;
	}

	public String getVersante() {
		return versante;
	}

	public void setVersante(String versante) {
		this.versante = versante;
	}

	/*
	 * private Object datiSpecifici = null;
	 * 
	 * @ManagedProperty("#{debitiService}") private DebitiService debitiService;
	 * 
	 * private HashMap<Class<?>,Object> map = null;
	 * 
	 * @PostConstruct public void init(){ map = new HashMap<>();
	 * 
	 * map. put(Integer.class, Integer.valueOf(0)); map. put(Long.class,
	 * Long.valueOf(0)); map. put(Boolean.class,Boolean.FALSE); map.
	 * put(Double.class,Double.valueOf(0d)); map
	 * .put(Float.class,Float.valueOf(0f)); //map.
	 * put(Character.class,"".charAt(0)); map.put(String.class, null);
	 * 
	 * getDatiSpecifici(); }
	 * 
	 * 
	 * 
	 * public void setDatiSpecifici(Object datiSpecifici) { this.datiSpecifici =
	 * datiSpecifici; }
	 * 
	 * 
	 * public DebitiService getDebitiService() { return debitiService; }
	 * 
	 * 
	 * public void setDebitiService(DebitiService debitiService) {
	 * this.debitiService = debitiService; }
	 * 
	 * 
	 * public Object getDatiSpecifici(){
	 * 
	 * if(datiSpecifici==null){
	 * 
	 * ContestoPagamento c = getDebitiService().getSelectedCategoria();
	 * 
	 * Class<?> clazz =
	 * getClassName(c.getAreaCategoria(),c.getIdCategoria()+""); try {
	 * datiSpecifici = clazz.newInstance();
	 * 
	 * initialize(datiSpecifici);
	 * 
	 * 
	 * ((Opccd)datiSpecifici).setAnno(2018); } catch (IllegalAccessException |
	 * InstantiationException e) { // TODO Blocco catch generato automaticamente
	 * e.printStackTrace(); }
	 * 
	 * } return datiSpecifici; }
	 * 
	 * 
	 * public Class<?> getClassName(String area, String id){ String className =
	 * Settings.getProperty(area+id);
	 * 
	 * Class<?> clazz = null;
	 * 
	 * try { clazz = Class.forName(className); } catch (ClassNotFoundException
	 * e) { e.printStackTrace(); }
	 * 
	 * return clazz;
	 * 
	 * }
	 * 
	 * 
	 * public void initialize(Object object) throws IllegalArgumentException,
	 * IllegalAccessException { if(object==null || object.getClass()==null ||
	 * object.getClass().getDeclaredFields()== null ||
	 * object.getClass().getDeclaredFields().length==0) return;
	 * 
	 * Field[] fields = object.getClass().getDeclaredFields();
	 * 
	 * for (Field field : fields) { String fieldName = field.getName(); Class<?>
	 * fieldClass = field.getType();
	 * 
	 * // skip primitives if (fieldClass.isPrimitive()) { continue; }
	 * 
	 * // skip if not in packages
	 * 
	 * 
	 * // allow access to private fields boolean isAccessible =
	 * field.isAccessible(); field.setAccessible(true); boolean salt = false;
	 * Object fieldValue = field.get(object); if (fieldValue == null) { try {
	 * if(!map.containsKey(fieldClass)){ field.set(object,
	 * fieldClass.newInstance()); }else{
	 * 
	 * field.set(object, map.get(fieldClass) );
	 * 
	 * salt = true;
	 * 
	 * } } catch (IllegalArgumentException | IllegalAccessException |
	 * InstantiationException | SecurityException e) {
	 * System.err.println("Could not initialize " + fieldClass.getSimpleName());
	 * } } else { }
	 * 
	 * fieldValue = field.get(object);
	 * 
	 * // reset accessible field.setAccessible(isAccessible); if(salt)continue;
	 * // recursive call for sub-objects initialize(fieldValue);//, packages);
	 * 
	 * // field.set(object, null); }
	 * 
	 * }
	 */

	/*
	 * 
	 * try { switch (type) { case INT: field.setInt(component,
	 * Integer.parseInt(data[i])); break; case DOUBLE:
	 * field.setDouble(component, Double.parseDouble(data[i])); break; case
	 * BYTE: field.setByte(component, Byte.parseByte(data[i])); break; case
	 * SHORT: field.setShort(component, Short.parseShort(data[i])); break; case
	 * LONG: field.setLong(component, Long.parseLong(data[i])); break; case
	 * FLOAT: field.setFloat(component, Float.parseFloat(data[i])); break; case
	 * BOOLEAN: field.setBoolean(component, Boolean.parseBoolean(data[i]));
	 * break; case CHAR: field.setChar(component, data[i].charAt(0)); break;
	 * case JAVALANGSTRING: field.set(component, data[i]); break; } } catch
	 * (Exception ex) { ex.printStackTrace(); } }
	 */

}
