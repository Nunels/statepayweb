package it.sogei.admpay.service.handler;

import it.sogei.data.DataUtilFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

public class ServiceLogHandler implements SOAPHandler<SOAPMessageContext> { 
	
	
	Logger log  = Logger.getLogger(ServiceLogHandler.class);


	@Override public Set<QName> getHeaders() {
		return Collections.emptySet(); 
		// TODO Auto-generated method stub return null; 
	} 

	@Override public void close(MessageContext arg0) { 
		// TODO Auto-generated method stub 
	} 

	@Override public boolean handleFault(SOAPMessageContext arg0) { 
		SOAPMessage message= arg0.getMessage(); 
		try { 
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			message.writeTo(baos); 
			
			log.info(DataUtilFactory.prettyPrintXml(baos.toByteArray())); 
		
		}catch (SOAPException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		}catch (IOException e) { 
			// TODO Auto-generated catch block 
			e.printStackTrace(); 
		} 
		return true; 
	} 
	@Override public boolean handleMessage(SOAPMessageContext arg0) {
		SOAPMessage message= arg0.getMessage(); 
		boolean isOutboundMessage= (Boolean)arg0.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY); 
		if(isOutboundMessage){ 
			log.info("\nOUTBOUND MESSAGE"); 
		}else{ 
			log.info("\nINBOUND MESSAGE"); 
		} 
		try { 
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			message.writeTo(baos); 
			
			log.info(DataUtilFactory.prettyPrintXml(baos.toByteArray()));
		} catch (SOAPException e) { e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); } 

		return true; 
	} 
} 