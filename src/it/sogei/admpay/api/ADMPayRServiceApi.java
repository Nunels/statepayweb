package it.sogei.admpay.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.admpay.model.AdmPay;
import it.sogei.admpay.model.AdmPayCUResult;
import it.sogei.admpay.model.AdmPayData;
import it.sogei.admpay.model.AdmPayList;
import it.sogei.admpay.model.AdmPayRT;
import it.sogei.admpay.model.AdmPayRTList;
import it.sogei.admpay.model.AdmPayWelcome;
import it.sogei.pay.system.model.external.servizi.ServiziData;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
@Api(value = "/", description = "")
public interface ADMPayRServiceApi  {

    @POST
    @Path("/ADMPayRService/selectAdmPay")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPay", tags={  })
    public AdmPay selectAdmPay(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPay body);

    @POST
    @Path("/ADMPayRService/selectAdmPayByKey")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPayByKey", tags={  })
    public AdmPay selectAdmPayByKey(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPay body);

    @POST
    @Path("/ADMPayRService/selectAdmPays")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPays", tags={  })
    public AdmPayData selectAdmPays(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayData body);

    @POST
    @Path("/ADMPayRService/selectAdmPaysByKey")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPays", tags={  })
    public AdmPayData selectAdmPaysByKey(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayData body);

    @POST
    @Path("/ADMPayRService/servicesList")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "servicesList", tags={  })
    public ServiziData servicesList(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);

    @POST
    @Path("/insertAdmPayRT")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "insertAdmPayRT", tags={  })
    public AdmPayCUResult insertAdmPayRT(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayRT body);
  
    @POST
    @Path("/selectAdmPayRTByDocID")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPayRTByDocID", tags={  })
    public AdmPayRTList selectAdmPayRTByDocID(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayRT body);

    @POST
    @Path("/selectAdmPayRTByID")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPayRTByID", tags={  })
    public AdmPayRTList selectAdmPayRTByID(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayRT body);
  
    @GET
    @Path("/ADMPayRService/welcomeTest")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "Welcome Test.", tags={  })
    public AdmPayWelcome welcomeTest(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);
    
    @POST
    @Path("/selectAdmPayRTByIdPagoPA")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "selectAdmPayRTByID", tags={  })
    public AdmPayList selectAdmPayByIdPagoPA(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, AdmPayRT body);
}

