package it.sogei.admpay.client;

import it.sogei.data.DataUtilFactory;
import it.sogei.ssi.util.AppConf;
import it.sogei.util.rest.client.ClientRest;
import it.sogei.util.rest.client.Response;
import it.sogei.util.rest.client.impl.ClientHttpConnectionUrlRest;

import java.net.MalformedURLException;
import java.util.Hashtable;

public class ADMPayRClient {

	
	
	public ClientRest client = null;
	
	public enum  MediaType {
		json  ( "application/json"),
		xml  ("application/xml");
		
		private String mediaType = null;
		
		MediaType (String mediaType){
			this.mediaType = mediaType;
		}
		
		public String value(){
			return this.mediaType;
		}
		
		
	}
	private String mediaType = null;


	public ADMPayRClient(String contentType,String pathService) {
		
		
		
		this.mediaType = contentType;
		try {
			 client=new ClientHttpConnectionUrlRest(AppConf.getConfig().getHostService(), pathService);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}
	
	
	
	public <T> Response restGlobalCall(String contentType,String language,T  input, String nomeServizio) throws Exception{
		
		Response response=null;
		
		try{
			
		    String inputString= input!=null?trasform(input):"";

			Hashtable<String, String> headerParams=new Hashtable<String, String>(); 
			headerParams.put("Accept-Language", language);
			headerParams.put("Content-Type", contentType);
			
			headerParams.put(AppConf.getConfig().getHeaderKey1(), AppConf.getConfig().getHeaderValue1());
			headerParams.put(AppConf.getConfig().getHeaderKey2(), AppConf.getConfig().getHeaderValue2());
			
			response = client.post(nomeServizio, contentType, headerParams, inputString, contentType);
		
		    checkResponse(response);
		
			
		}catch(Exception e ){
			e.printStackTrace();
		}
		
	   return response;
	}
	
	
	
	
	private <T> void check(T object)throws Exception{
		if(object == null){
			throw new Exception("Errore object null");
		}
	}
	
	
	private void checkResponse(Response response)throws Exception{
		if(response==null){
			throw new Exception("Errore impossibile interpretare la response");
		}
		
		if(response.getStatus()!= 200  && response.getStatus()<500  && response.getStatus()!=400){
			throw new Exception("Errore nella chiamata al servizio, codice di ritorno : "+response.getStatus() );
		}
			
		
	}
	
	private <T> String trasform(T object)throws Exception{
		
		String requestString = null;
		
		if(mediaType.equals(MediaType.json.value()))
			requestString = new String(DataUtilFactory.trasformObjectToJsonString(object));
		else if(mediaType.equals(MediaType.xml.value())){
			requestString = new String(DataUtilFactory.trasformObjectToXmlString(object));
		}else{
			throw new Exception("Errore MediaType non consentito");
		}
		
		
				

		return requestString;
	}
	
	public <T> T populate(T object, byte[] bytes)throws Exception{
		
		T result = null;
		
		if(mediaType.equals(MediaType.json.value()))
			result = DataUtilFactory.populateObjectFromJson(object, bytes);
		else if(mediaType.equals(MediaType.xml.value())){
			result = DataUtilFactory.populateObjectFromXml(object, bytes);
		}else{
			throw new Exception("Errore MediaType non consentito");
		}
		
		check(result);
		
		return result;
	}
	
  
	



}
