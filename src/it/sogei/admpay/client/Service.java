package it.sogei.admpay.client;

import it.dsi.ssi.client.AUDMClient;
import it.dsi.ssi.client.AUDMClient.MediaType;
import it.finanze.dogane.npad.ws.model.dto.NpadVerificaListaRequest;
import it.finanze.dogane.npad.ws.model.dto.NpadVerificaResponse;
import it.sogei.admpay.model.SystemConfig;
import it.sogei.data.DataUtilFactory.DOCUMENT;
import it.sogei.dsi.ssi.npad.client.NpadRClient;
import it.sogei.pay.system.client.SogeiPaySystemClient;
import it.sogei.pay.system.client.SogeiPaySystemClientFactory;
import it.sogei.pay.system.client.exception.NotSupportClientType;
import it.sogei.pay.system.util.TLSSocketFactory;
import it.sogei.ssi.util.AppConf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Hashtable;

import javax.net.ssl.KeyManagerFactory;

import org.apache.log4j.Logger;

import ticket.ws.service.ProcessaWebMail;
import ticket.ws.service.ProcessaWebMailProxy;

public class Service {

	private SystemConfig config = null;
	private String nodoContentType = null;
	Logger log = Logger.getLogger(Service.class);

	// private static final String prime1 =
	// "24053791538606908124360654530379389396587002402169971998331041515439068386790199306037187642735237593268185254657860559364261933903409795075912840606431816265269723315884908386881528800030894105971949968994245383733932105965337667158331774940604778974607289106324061698337053538469587665393200479954331185808104883076911232760793243784831598354136818534715251160985242292787826305390583186916929394418404609459965964955461140664294464431581716399598807570411829858587803866891018523094386632303475714683710636141844261924192193719847214483986416494484991345710000304737337191329478261893655207472475402501781585203037";
	// private static final String prime2 =
	// "6063839088427163184765650863577559109177061526166352276067079897107831057070201269335697992822406033568854095744597555235421559995735922221594537862177486683507625436206001766515189294338869236130213538175638514876218748549384424792499292011617609255706343523608987878281314593054019843050997700221988698699353467344345132125843391333912233485266646426081678176193100581113661584733496543637245787401301453766318340986200088013268502696055956526446949078396813813588185210155864778016413160989431098865278485185408066245939298444083396680194133902511578437079893578029970044786236655105430525634865871734926799670717";

	// public static final String nodoEndPointPrefix =
	// "http://tfpdd-dsi-ssi:8080/openspcoop/PD/SPCPortalePagamenti/SPCDigitPa/SPCPagamentiTelematiciRPT/";

	public String getNodoContentType() {
		return nodoContentType;
	}

	public void setNodoContentType(String nodoContentType) {
		this.nodoContentType = nodoContentType;
	}

	public Service() {

		config = AppConf.getConfig();
	}

	/*
	 * public SogeiPaySystemClient nodoClient(){ SogeiPaySystemServiceSuper
	 * proxy = new SogeiPaySystemServiceSuper(); SogeiPaySystemService
	 * spsService = proxy.getSogeiPaySystemService();
	 * 
	 * BindingProvider bp = (BindingProvider)spsService;
	 * bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
	 * endPointGNodo);
	 * 
	 * 
	 * 
	 * if(wssec){ WSSecurityClient wsclient = new
	 * WSSecurityClient(AppUtil.getAppPath()+"WEB-INF/client_sign.properties",
	 * "/prod/doganeprj/conf/SportelloUnicoKeyStore.jks", "123456",
	 * "/prod/doganeprj/conf/SportelloUnicoKeyStore.jks", "123456", true);
	 * //WSSecurityClient.DEBUG = true;s
	 * wsclient.addUsernameTokenAndSignature(spsService,
	 * AuthUtil.getUser().getScreenName(),"soapui"); } SogeiPaySystemClient
	 * sclient =SogeiPaySystemClientFactory.getSoapClient(spsService,
	 * endPointGNodo);
	 * 
	 * return sclient; }
	 */

	public ProcessaWebMail getAssistenzaClient() {
		log.info(config.getEndpointAssistenza());

		ProcessaWebMailProxy proxy = new ProcessaWebMailProxy(
				config.getEndpointAssistenza());

		return proxy.getProcessaWebMail();
	}

	public AUDMClient getAUDMClient() {

		String sslContextProtocol = "TLS";
		if (config.isTls12Esclusivo()) {
			sslContextProtocol = "TLSv1.2";
		}

		if (config.getEndPointAudmHost() == null
				|| config.getEndPointAudmPath() == null)
			return null;

		AUDMClient client = null;
		if (config.getCertPath() != null && config.getCertPwd() != null) {
			client = new AUDMClient(config.getEndPointAudmHost(),
					config.getEndPointAudmPath(), sslContextProtocol,
					config.getCertPath(), config.getCertPwd(),
					config.getCertType(), MediaType.json);
		} else {
			client = new AUDMClient(config.getEndPointAudmHost(),
					config.getEndPointAudmPath(), MediaType.json);

		}

		return client;
	}

	public SogeiPaySystemClient nodoClient() {
		/*
		 * SogeiPaySystemServiceSuper proxy = new SogeiPaySystemServiceSuper();
		 * SogeiPaySystemService spsService = proxy.getSogeiPaySystemService();
		 * 
		 * BindingProvider bp = (BindingProvider)spsService;
		 * bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
		 * config.getEndPointNodo());
		 */

		Hashtable<String, String> header = new Hashtable<>();
		header.put(config.getHeaderKey1(), config.getHeaderValue1());
		header.put(config.getHeaderKey2(), config.getHeaderValue2());

		nodoContentType = "application/json";

		if (config.getEndPointNodo() != null
				&& config.getEndPointNodo().startsWith("https")) {
			// sslConfig(spsService, certPath, certPwd, certType);
		}
		log.info(config.getHostServiceNodo() + "/"
				+ config.getPathRestServiceNodo());

		// SogeiPaySystemClient sclient
		// =SogeiPaySystemClientFactory.getSoapClient(spsService,
		// config.getEndPointNodo());
		SogeiPaySystemClient sclient = null;
		try {
			sclient = SogeiPaySystemClientFactory.getRestClient(
					config.getHostServiceNodo(),
					config.getPathRestServiceNodo(), DOCUMENT.JSON, null,
					header);

		} catch (MalformedURLException | NotSupportClientType e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}

		return sclient;
	}

	/*
	 * public SogeiPaySystemClient nodoClientDownload(){
	 * 
	 * 
	 * Hashtable<String , String> header = new Hashtable<>();
	 * header.put(config.getHeaderKey1(),config.getHeaderValue1());
	 * header.put(config.getHeaderKey2(),config.getHeaderValue2());
	 * 
	 * nodoContentType = "application/json";
	 * 
	 * if(config.getEndPointNodo()!=null &&
	 * config.getEndPointNodo().startsWith("https")){ // sslConfig(spsService,
	 * certPath, certPwd, certType); }
	 * log.info(config.getHostServiceNodoDownload
	 * ()+"/"+config.getPathRestServiceNodo());
	 * 
	 * //SogeiPaySystemClient sclient
	 * =SogeiPaySystemClientFactory.getSoapClient(spsService,
	 * config.getEndPointNodo()); SogeiPaySystemClient sclient = null; try {
	 * sclient =
	 * SogeiPaySystemClientFactory.getRestClient(config.getHostServiceNodoDownload
	 * (), config.getPathRestServiceNodo(), DOCUMENT.JSON,null,header);
	 * 
	 * } catch (MalformedURLException | NotSupportClientType e) { // TODO Blocco
	 * catch generato automaticamente e.printStackTrace(); }
	 * 
	 * return sclient; }
	 */

	private void sslConfig(Object wsProxy, String certPath, String certPwd,
			String certType) {

		/*
		 * List handlerList =
		 * ((BindingProvider)wsProxy).getBinding().getHandlerChain(); if
		 * (handlerList == null) handlerList = new ArrayList();
		 * 
		 * handlerList.add(new ServiceLogHandler());
		 * bp.getBinding().setHandlerChain(handlerList);
		 */

		TLSSocketFactory ssl = null;
		String sslContextProtocol = "TLS";

		KeyStore keyStore;

		KeyManagerFactory keyManagerFactory = null;
		try {
			keyStore = KeyStore.getInstance(certType);

			keyStore.load(new FileInputStream(new File(certPath)),
					certPwd.toCharArray());

			keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory
					.getDefaultAlgorithm());
			keyManagerFactory.init(keyStore, certPwd.toCharArray());

			ssl = new TLSSocketFactory(sslContextProtocol,
					keyManagerFactory.getKeyManagers(), null);

		} catch (KeyManagementException | UnrecoverableKeyException
				| IOException | KeyStoreException | CertificateException
				| NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*
		 * SecurityClient.DEBUG=false; SecurityClient sClient =new
		 * SecurityClient(wsProxy,ssl.getSslSocketFactory());
		 */

	}

	public NpadVerificaResponse verificaAutorizzazione(String user) {

		// NPAD
		// NpadRClient npadClient = new NpadRClient(npadHost, npadPath,
		// NpadRClient.MediaType.json);
		NpadRClient npadClient = null;
		if (config.getCertPath() != null && config.getCertPwd() != null) {
			npadClient = new NpadRClient(config.getNpadHost(),
					config.getNpadPath(), "TLSv1.2", config.getCertPath(),
					config.getCertPwd(), "PKCS12", NpadRClient.MediaType.xml);
		} else {
			npadClient = new NpadRClient(config.getNpadHost(),
					config.getNpadPath(), NpadRClient.MediaType.xml);

		}

		NpadVerificaListaRequest request = new NpadVerificaListaRequest();

		request.setUtente(user);
		request.setNomeServizio(config.getNomeServizio());
		request.getLCodiciAutorizzazione()
				.add(config.getCodiceAutorizzazione());

		NpadVerificaResponse npadResponse = null;

		try {
			npadResponse = npadClient.verificaListaAutorizzazioni(request);
		} catch (Exception e) {
			// TODO Blocco catch generato automaticamente
			e.printStackTrace();
		}

		// npadResponse== null ||
		// npadResponse.getLAutorizzazioni().size()==0
		// npadResponse.getLAutorizzazioni().size()>0 autorizzato

		return npadResponse;
	}

}
