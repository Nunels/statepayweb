package it.sogei.ssi.util;

import it.sogei.admpay.model.Dominio;
import it.sogei.admpay.model.SystemConfig;
import it.sogei.admpay.model.Utente;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

public class AppConf {

	public final static String VERSION = "1.0";
	public final static String ENV_ROOT_VAR = "PROOT_VAR";
	public final static String ROOT_VAR = "C:/doganeprj/";

	private final static String MESSAGE_FILE_PATH = "admpay/messages_it.properties";

	public static final String SETTING_PATH = "admpay/config.properties";
	public static final String LOGGING_PATH = "sps/properties/log4j.properties";

	public final static String APP_NAME = "GESTIONE DEI DATI DEI CONTATORI DI ENERGIA ELETTRICA CERTIFICATI DAI LABORATORI ACCREDITATI";
	public final static String LOGIN_TITLE = "GESTIONE DEI DATI DEI CONTATORI DI ENERGIA ELETTRICA CERTIFICATI DAI LABORATORI ACCREDITATI";

	public final static String HOME_TITLE = "Agenzia delle dogane e dei Monopoli - Home";

	public final static String USER_BEAN = "currentUser";

	public final static String LOGIN_PATH = "/error.xhtml";
	public final static String ERROR_PATH = "/error.xhtml";

	public static final String SERVICE = "ADMPAYSYS";
	public static final String PRINCIPAL_DOMAIN = "97210890584";
	public static final String UNAUTHENTICATED = "UNAUTHENTICATED";

	public String appName = APP_NAME;
	private String userBean = USER_BEAN;
	private String loginTitle = LOGIN_TITLE;
	private String loginPath = LOGIN_PATH;
	private String errorPath = ERROR_PATH;
	private String version = VERSION;

	private String unauthenticated = UNAUTHENTICATED;

	private boolean isBenchmark = false;

	//
	private static List<SelectItem> listItem = null;
	private static Map<String, String> tuttiIdomini = null;

	public static String getProjectRoot() {

		// if (System.getProperty(ENV_ROOT_VAR).endsWith("/")) {
		// return System.getProperty(ENV_ROOT_VAR);
		// } else {
		// return System.getProperty(ENV_ROOT_VAR) + "/";
		// }
		return ROOT_VAR;
	}

	public static String getLogConfFile() {

		return getProjectRoot() + LOGIN_PATH;
	}

	public static String getConfFile() {

		return getProjectRoot() + SETTING_PATH;
	}

	public static String getMessageFile() {

		return getProjectRoot() + MESSAGE_FILE_PATH;
	}

	public String getUserBean() {
		return userBean;
	}

	public String getLoginTitle() {
		return loginTitle;
	}

	public String getVersion() {
		return version;
	}

	public String getLoginPath() {
		return loginPath;
	}

	public String getAppName() {
		return appName;
	}

	public String getConfigFilePath() {
		return SETTING_PATH;
	}

	public String getErrorPath() {
		return errorPath;
	}

	public static final Dominio getDominio() {

		Dominio dominio = new Dominio();

		String d = null;
		if (AppUtil.getDominio().indexOf(".") >= 0) {
			d = (AppUtil.getDominio().split("\\."))[1].toUpperCase();
		}
		if (Settings.getProperty("ID_DOMINIO_PA_" + d) == null || d == null) {
			d = "ADM";
		}

		dominio.setIdIntermediarioPA(Settings
				.getProperty("ID_INTERMEDIARIO_PA"));

		dominio.setIdStazioneIntermediarioPA(Settings
				.getProperty("ID_STAZIONE_PA"));

		dominio.setDeominazione(Settings.getProperty("DENOMINAZIONE_PA"));

		dominio.setIdDominio(Settings.getProperty("ID_DOMINIO_PA"));

		dominio.setPwdPA(Settings.getProperty("PWD_PA"));

		dominio.setTipoFirma(Settings.getProperty("TIPO_FIRMA_RT"));

		boolean test = Boolean.parseBoolean(Settings.getProperty("TEST_PSP"));

		if (test) { // WISP 2
			dominio.setWisp20(true);
			dominio.setIdPsp(Settings.getProperty("ID_PSP"));
			dominio.setIdIntermediarioPsp(Settings
					.getProperty("ID_INTERMEDIARIO_PSP"));
			dominio.setIdCanalePsp(Settings.getProperty("ID_CANALE_PSP"));
		}

		dominio.setIbanAccredito(Settings.getProperty("IBAN_ACCREDITO"));
		dominio.setBicAccredito(Settings.getProperty("BIC_ACCREDITO"));
		dominio.setDatiRiscossione(Settings.getProperty("DATI_RISCOSSIONE"));

		dominio.setLayout((Settings.getProperty("LAYOUT_" + d)));

		dominio.setContent(Settings.getProperty("CONTENT_" + d));

		return dominio;

	}

	public static final SystemConfig getConfig() {
		SystemConfig config = new SystemConfig();

		config.setAmbienteSuCuiGiro(Settings.getProperty("ambiente"));

		config.setEndPointNodo(Settings.getProperty("END_POINT_NODO"));

		config.setHostServiceNodo(Settings.getProperty("END_POINT_NODO_HOST"));
		config.setPathRestServiceNodo(Settings
				.getProperty("END_POINT_NODO_PATH"));

		config.setEndpointAssistenza(Settings
				.getProperty("END_POINT_ASSISTENZA"));

		config.setHostService(Settings.getProperty("HOST_SERVICE"));

		config.setADMPayRServiceApi(Settings.getProperty("ADMPayRService"));
		config.setADMPayCartRServiceApi(Settings
				.getProperty("ADMPayCartRService"));
		config.setADMPayReceiverRServiceApi(Settings
				.getProperty("ADMPayReceiverRService"));

		config.setHeaderKey1(Settings.getProperty("HEADER_KEY1"));
		config.setHeaderValue1(Settings.getProperty("HEADER_VALUE1"));
		config.setHeaderKey2(Settings.getProperty("HEADER_KEY2"));
		config.setHeaderValue2(Settings.getProperty("HEADER_VALUE2"));

		config.setCertPath(Settings.getProperty("CLIENT_CERT_PATH"));
		config.setCertPwd(Settings.getProperty("CLIENT_CERT_PWD"));
		config.setCertType(Settings.getProperty("CLIENT_CERT_TYPE"));

		if (Settings.getProperty("NPAD_IS_ENABLE") != null) {
			try {
				config.setNpadIsEnable(Boolean.parseBoolean(Settings
						.getProperty("NPAD_IS_ENABLE")));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (Settings.getProperty("GENERATOR_DS") != null) {
			try {
				config.setGeneratorDSEnable(Boolean.parseBoolean(Settings
						.getProperty("GENERATOR_DS")));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		config.setNpadHost(Settings.getProperty("WS_NPAD_HOSTSERVICE"));
		config.setNpadPath(Settings.getProperty("WS_NPAD_PATHSERVICE"));
		config.setNomeServizio(Settings.getProperty("NOME_SERVIZIO"));
		config.setCodiceAutorizzazione(Settings
				.getProperty("CODICE_AUTORIZZAZIONE"));

		if (Settings.getProperty("AUDM_IS_ENABLE") != null) {
			try {
				config.setAudmIsEnable(Boolean.parseBoolean(Settings
						.getProperty("AUDM_IS_ENABLE")));

				if (Settings.getProperty("AUDM_ISTANCE") != null) {
					config.setAudmIstance(Settings.getProperty("AUDM_ISTANCE"));
				}

				config.setEndPointAudmHost(Settings
						.getProperty("END_POINT_AUDM_HOST"));
				config.setEndPointAudmPath(Settings
						.getProperty("END_POINT_AUDM_PATH"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (Settings.getProperty("UTENTE_ENABLE") != null) {
			try {

				config.setUtenteDiTest(Boolean.parseBoolean(Settings
						.getProperty("UTENTE_ENABLE")));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		config.setHostServiceNodoDownload(Settings
				.getProperty("END_POINT_NODO_HOST_DOWNLOAD"));

		config.setTipoAgenzia(Settings.getProperty("TIPOAGENZIA"));
		config.setSystem(Settings.getProperty("SYSTEM"));
		config.setComponent(Settings.getProperty("COMPONENT"));
		config.setExpRegUserMatch(Settings.getProperty("EXPREGUSERMATCH"));
		return config;
	}

	public static Utente getUtente() {

		Utente utente = new Utente();

		if (Settings.getProperty("UTENTE_ENABLE") != null) {
			try {

				utente.setCfPagatore(Settings.getProperty("UTENTE"));
				utente.setEmailPagatore(Settings.getProperty("UTENTE_MAIL"));
				utente.setDenominazione(Settings
						.getProperty("UTENTE_DENOMINAZIONE"));
				if (Settings.getProperty("ID_SERVIZIO") != null) {
					try {
						utente.setIdServizio(Long.parseLong(Settings
								.getProperty("ID_SERVIZIO")));

					} catch (Exception e) {
						// noop
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return utente;
	}

	public static List<SelectItem> tuttiIdomini() {
		// if(dominio==null){
		getDominio();
		// }
		// tuttiIdomini = new HashMap<String,String>();
		listItem = new ArrayList<SelectItem>();
		Map<String, String> l = Settings
				.prefixedValueProperties("ID_DOMINIO_PA_");

		for (String key : l.keySet()) {

			System.out.println(l.get(key) + " "
					+ Settings.getProperty("DENOMINAZIONE_PA_" + key));

			// tuttiIdomini.put(l.get(key),
			// Settings.getProperty("DENOMINAZIONE_PA_"+key));
			SelectItem item = new SelectItem();
			item.setValue(l.get(key));
			item.setLabel(Settings.getProperty("DENOMINAZIONE_PA_" + key));

			listItem.add(item);
		}

		return listItem;

	}

	public static long getActiveIndexSogei(List<SelectItem> list) {

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getLabel().equals("SOGEI")) {
				return new Long(i);
			}
		}
		return new Long(0);
	}

}
