package it.sogei.ssi.util.exporter;

import it.sogei.ssi.util.exporter.core.DataExporter;


public  class RawBytesExporter extends DataExporter {
	   
	   private byte[] byteArray;
	   private String contentType;
	   
	   public RawBytesExporter(String fileNameWithExtension){
	      super(fileNameWithExtension);
	   }
	   
	   public void setBytes(byte[] byteArray){
		   this.byteArray = byteArray;
	   }
	   
	   public void setContentType(String contentType){
		   this.contentType = contentType;
	   }
	   
	   @Override
	   public String getContentType() {
	      return this.contentType;
	   }

	   @Override
	   public byte[] getBytes() throws Exception {
	      return  this.byteArray;
	   }

	public byte[] getByteArray() {
		return byteArray;
	}

	public void setByteArray(byte[] byteArray) {
		this.byteArray = byteArray;
	}

	}