package it.sogei.ssi.util.exporter.core;

import javax.faces.context.FacesContext;
import it.sogei.ssi.util.AppUtil;
import org.primefaces.PrimeFaces;


public abstract class DataExporter {
	
	public final static String ExelContentType = "application/vnd.ms-excel";
	public final static String XmlContentType = "application/xml";
	public final static String PdfContentType = "application/pdf";
	public final static String TextPlainContentType = "text/plain";
	public final static String CsvContentType = "text/csv";
	
	//private static Log _log = LogFactoryUtil.getLog(DataExporter.class);
	
    private String fileName;

    public DataExporter(String fileName) {
       this.fileName = fileName;
    }
    
  //  public abstract void doExport();
    public abstract byte[] getBytes() throws Exception;
    public abstract String getContentType();

    public void export() throws Exception {
      // doExport();
       
       long exportId = System.currentTimeMillis();
       
       AppUtil.setInRequestMap(ExportResource.PARAM_NAME_EXPORT_ID, exportId);
       AppUtil.setInRequestMap(ExportResource.PARAM_NAME_EXPORT_TYPE, getContentType());
       AppUtil.setInRequestMap(ExportResource.PARAM_NAME_EXPORT_FILENAME, this.fileName);
       
       AppUtil.setInSessionMap(ExportResource.PARAM_NAME_EXPORT_DATA + exportId, getBytes());

       String url = AppUtil.getExpressionValue("resource['bon:export']").toString();
      
       //_log.error(getContentType());
      // _log.error(url);
		//FacesContext.getCurrentInstance().getResponseWriter().flush();
		
       PrimeFaces.current().executeScript("window.location.href = '" + url + "'");
 
    }

    

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
    
 }
