package it.sogei.ssi.util.exporter.core;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;

public class CoreResourceHandler extends ResourceHandlerWrapper {

    public static final String LIBRARY_NAME = "bon";

    private ResourceHandler wrapped;

    public CoreResourceHandler(ResourceHandler wrappedResourceHandler) {
       this.wrapped = wrappedResourceHandler;
    }

    @Override
    public Resource createResource(String resourceName, String libraryName) {
       if (LIBRARY_NAME.equals(libraryName)) {
          if (ExportResource.RESOURCE_NAME.equals(resourceName)) {
             return new ExportResource();
          }
       }

       return wrapped.createResource(resourceName, libraryName);
    }

    @Override
    public boolean libraryExists(String libraryName) {
       if (LIBRARY_NAME.equals(libraryName)) {
          return true;
       } else {
          return super.libraryExists(libraryName);
       }
    }

    @Override
    public ResourceHandler getWrapped() {
       return wrapped;
    }

	public void setWrapped(ResourceHandler wrapped) {
		this.wrapped = wrapped;
	}

 }
