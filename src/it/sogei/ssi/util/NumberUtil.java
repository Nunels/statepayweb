package it.sogei.ssi.util;

public class NumberUtil {
	
	 public static double approx(double input, int decLen){
	       double val = 0;
	       double result = 0;

	       if (input<0)
	             val = (-1)*input;
	       else
	             val = input;

	    double cost = 0.5;
	    double len = decLen;
	    double z = Math.pow(10, len);

	   // double inc = cost / z;
	   double s = 0.0001 / z;
	   double inc = cost / z +s;

	    if (input<0)
	       result = (-1)*(Math.floor((val + inc)*z)/z);
	    else
	       result = Math.floor((val + inc)*z)/z;

	    return result;
	  }

}
