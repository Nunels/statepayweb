package it.sogei.ssi.util;

 
import it.gov.digitpa.schemas._2011.pagamenti.CtRicevutaTelematica;
import it.gov.digitpa.schemas._2011.pagamenti.ObjectFactory;
import it.sogei.data.DataUtilFactory;
import it.sogei.pay.system.util.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.xml.datatype.XMLGregorianCalendar;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class Pdf {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	public static void main(String[] args) throws DocumentException, IOException {
		// TODO Auto-generated method stub

		PdfReader reader = new PdfReader("d:/doganeprj/templateADMPay.pdf");
		
		CtRicevutaTelematica rt= new CtRicevutaTelematica();
		
		byte[] rtfile =FileUtils.readFileToByteArray(new File("d:/doganeprj/rt.xml"));
		
		
		ObjectFactory objFactory  = new ObjectFactory();
		
		rt = DataUtilFactory.populateObjectFromXml(rt, rtfile , objFactory.createRT(rt).getName());

		
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        PdfStamper stamper = new PdfStamper(reader, bos);
        AcroFields form = stamper.getAcroFields();
 
         
        form.setField("ADMPayForm[0].Subform[0].singoloImportoPagato[0]", rt.getDatiPagamento().getImportoTotalePagato().toString());
        form.setField("ADMPayForm[0].Subform[0].riferimentoDataRichiesta[0]",xmlGregorianCalendar2String( rt.getDataOraMessaggioRicevuta()) );
        form.setField("ADMPayForm[0].Subform[0].identificativoDominio[0]", rt.getDominio().getIdentificativoDominio());
        form.setField("ADMPayForm[0].Subform[0].CodiceContestoPagamento[0]",rt.getDatiPagamento().getCodiceContestoPagamento());
        form.setField("ADMPayForm[0].Subform[0].identificativoUnivocoVersamento[0]", rt.getDatiPagamento().getIdentificativoUnivocoVersamento());
        form.setField("ADMPayForm[0].Subform[0].codiceIdentificativoUnivoco[0]", rt.getIstitutoAttestante().getDenominazioneAttestante());
        form.setField("ADMPayForm[0].Subform[0].identificativoUnivocoRiscossione[0]",rt.getIdentificativoMessaggioRicevuta()); 
        form.setField("ADMPayForm[0].Subform[0].TextField1[0]",  xmlGregorianCalendar2String(rt.getDataOraMessaggioRicevuta())); 

      
        form.setField("ADMPayForm[0].Subform[0].Table1[0].HeaderRow[0].Cell1[0]", "PROGRESSIVO");      
        form.setField("ADMPayForm[0].Subform[0].Table1[0].HeaderRow[0].Cell2[0]", " ");  
        form.setField("ADMPayForm[0].Subform[0].Table1[0].HeaderRow[0].Cell3[0]", "N.RO QUIETANZA");
        form.setField("ADMPayForm[0].Subform[0].Table1[0].HeaderRow[0].Cell4[0]", "DATA");
 

        for(int i=1; i<=10;i++)
        {    
        	
        	if (i<=rt.getDatiPagamento().getDatiSingoloPagamento().size()*2){
        		
        		 if (i%2!=0){
        		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Cell1[0]", Integer.toString(i));	
        		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Cell2[0]", rt.getDatiPagamento().getDatiSingoloPagamento().get(i-1).getCausaleVersamento().substring(rt.getDatiPagamento().getDatiSingoloPagamento().get(i-1).getCausaleVersamento().lastIndexOf("/")+1));	
        		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Cell3[0]", rt.getDatiPagamento().getDatiSingoloPagamento().get(i-1).getIdentificativoUnivocoRiscossione());	
        		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Cell4[0]", xmlGregorianCalendar2StringOnlyDate( rt.getDatiPagamento().getDatiSingoloPagamento().get(i-1).getDataEsitoSingoloPagamento()));	
        		 }
        		 else{
            		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Cell1[0]", "CAUSALE");	
            		 form.setField("ADMPayForm[0].Subform[0].Table1[0].Row"+i+"[0].Causale[0]",  rt.getDatiPagamento().getDatiSingoloPagamento().get(i-2).getCausaleVersamento());	
        		 }
        		 
        	}
                   	
        }
        
         
        form.setField("label", "label");
        stamper.setFormFlattening(true);
        stamper.close();
        reader.close();
        
        bos.toByteArray();  // in memoria
        
        File out= new File("d:/doganeprj/fileW.pdf");
        
      
			FileUtils.writeByteArrayToFile(out, bos.toByteArray());
	 
	}
 
public static String xmlGregorianCalendar2String(XMLGregorianCalendar cal){
		
		String str = "";
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		str = format.format(cal.toGregorianCalendar().getTime());
		return str;
	}


public static String xmlGregorianCalendar2StringOnlyDate(XMLGregorianCalendar cal){
	
	String str = "";
	SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	str = format.format(cal.toGregorianCalendar().getTime());
	return str;
}

}
