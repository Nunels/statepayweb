package it.sogei.ssi.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;

import it.sogei.admpay.model.AdmPay;
import it.sogei.admpay.model.exception.ADMPaySystemException;
import it.sogei.data.DataUtilFactory;

public class AdmPayUtil {

 
	public static Object getObject(AdmPay admPay , String type) throws ADMPaySystemException{

		Object obj  = null;
		Class<?> clazz = getClassName(admPay);
		
 		
		if(type.equals("application/xml")){
			obj= DataUtilFactory.populateObjectFromXml(clazz, admPay.getDatiSpecifici());
		}else if(type.equals("application/json")) {
			obj= DataUtilFactory.populateObjectFromJson(clazz, admPay.getDatiSpecifici());
		}else{
			throw new ADMPaySystemException();
		}
		
		
		return obj;
	}

	
	public static Object getObjectNoType(AdmPay admPay ) {

		Object obj  = null;
		Class<?> clazz = getClassName(admPay);
		
 		
		try{
			obj= DataUtilFactory.populateObjectFromJson(clazz, admPay.getDatiSpecifici());
			
		}catch (Exception e){
			e.printStackTrace();
		}
		if(obj==null){
			try{
				obj= DataUtilFactory.populateObjectFromXml(clazz, admPay.getDatiSpecifici());
				
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		
		
		return obj;
	}

	

	public static Class<?> getClassName(AdmPay admPay){
		String className = Settings.getProperty(admPay.getArea()+Integer.toString(admPay.getIdServizio()));
		
		Class<?> clazz = null;

		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) { 	e.printStackTrace(); }
		
		return clazz;

	}
	
	
	
	public static int getRandomValue(int min, int max){
		
		Random random = new Random();

		int randomNumber = random.nextInt(max + 1 - min) + min;
		
		return randomNumber;
	}
	
	public static Date getDataScadenza(){
		Calendar cal = Calendar.getInstance();
		//Date today = cal.getTime();
		cal.add(Calendar.YEAR, 1); 
		Date nextYear = cal.getTime();
		
		return nextYear;
	}

}
