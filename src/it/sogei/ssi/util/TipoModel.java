package it.sogei.ssi.util;

public enum TipoModel {

	BAR_MODEL("BAR MODEL","BarChartModel",true),
	HORIZONTAL_BAR_MODEL(" HORIZONTAL BAR MODEL","HorizontalBarChartModel",true),
	LINE_MODEL("LINE MODEL","LineChartModel",true),
	PIE_MODEL("PIE MODEL","PieChartModel",true),
	DONUT_MODEL(" DONUT MODEL","DonutChartModel",true);
	
	private String nome=null;
	private String tipo=null;
	private boolean compatibile;

	private TipoModel(String nome,String tipo,boolean compatibile) {
		this.nome = nome;
		this.tipo=tipo;
		this.compatibile=compatibile;
	}

	public boolean isCompatibile() {
		return compatibile;
	}

	public void setCompatibile(boolean compatibile) {
		this.compatibile = compatibile;
	}

	public String getNome() {
		return nome;
	}

	public String getTipo() {
		return tipo;
	}
	
	
}
